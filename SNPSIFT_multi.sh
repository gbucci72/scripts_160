#PBS -l select=1:app=java:ncpus=4:mem=16gb
#PBS -P 161 
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1

condativate 

zcat $targ | vcfbreakmulti |bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa|\
snpSift annotate /lustre1/genomes/hg19/annotation/dbSNP-146.vcf.gz | \
snpSift annotate /lustre1/genomes/hg19/annotation/clinvar_20160802.vcf.gz | \
snpSift annotate /lustre1/genomes/hg19/annotation/v75_CosmicCodingMuts.vcf.gz | \
snpSift dbnsfp -db /lustre1/genomes/hg19/annotation/dbNSFP2.9.txt.gz - | vcfsort| bgzip -f -c > ${targ%%.vcf.gz}.Anno.vcf.gz

tabix ${targ%%.vcf.gz}.Anno.vcf.gz 

condeactivate

###for i in `ls Merged.chr*.vcf.gz`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/SNPSIFTANNO.sh ; done
