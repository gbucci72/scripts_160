#PBS -l select=1:app=java:ncpus=4:mem=16gb
#PBS -P 161 
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1
name=$arg2


vcfkeepinfo ${targ} "VARTYPE" "KASP" "HS2_AC" "HLALOSS_AC" | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar \
annotate /lustre1/genomes/hg19/annotation/dbSNP-146.vcf.gz $targ | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar annotate /lustre1/genomes/hg19/annotation/clinvar_20160802.vcf.gz | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar annotate /lustre1/genomes/hg19/annotation/v75_CosmicCodingMuts.vcf.gz | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar dbnsfp -db /lustre1/genomes/hg19/annotation/dbNSFP2.9.txt.gz - | \
java -jar /usr/local/cluster/src/snpEff/snpEff.jar eff -c  /lustre1/tools/etc/snpEff.config -t -noLog -nodownload -v -lof GRCh37.75 | vcfsort| bgzip -f -c > ${targ%%.vcf.gz}.Anno.vcf.gz

tabix ${targ%%.vcf.gz}.Anno.vcf.gz 



###for i in `ls Merged.chr*.vcf.gz`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/SNPSIFTANNO.sh ; done
