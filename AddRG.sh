#!/bin/bash
BaseName=$1
echo $BaseName
Name=${BaseName}_hg19_MBC.bam
script_name=${BaseName}.sh
cat <<__EOF__> $script_name
HOST=`hostname`
echo $HOST

#PBS -N ${BaseName}
#PBS -l select=1:ncpus=4:mem=8g:app=java
#PBS -V
#PBS -o ${name}_${HOST}.log
#PBS -e ${name}_${HOST}.err
#PBS -q workq
#PBS -M bucci.gabriele@hsr.it
#PBS -m ae
#PBS -Wsandbox=private


cd $PWD

ulimit -l unlimited
ulimit -s unlimited

##echo -e "@RG\tID:$BaseName\tPU:1\tLB:$BaseName\tSM:$BaseName\tCN:CTGB\tPL:illumina\tSO:coordinate"> ${BaseName}.rg.txt
##samtools view -h $Name | cat ${BaseName}.rg.txt - | awk '{ if (substr($1,1,1)=="@") print; else printf "%s\tRG:Z:$BaseName\n",\$0; }'  | samtools view -Sb - > $BaseName.bam
samtools view -h $Name | awk '{ if (substr(\$1,1,1)=="@") print; else printf "%s\tRG:Z:$BaseName\n",\$0; }'  | samtools view -Sb - > $BaseName.bam

samtools index $BaseName.bam
echo $Basename "RG added"


__EOF__

qsub $script_name
