#PBS -l select=1:app=java:ncpus=4:mem=8gb
#PBS -P 160 
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
R1=$arg1
R2=${R1/_R1_/_R2_}
name=${R1%%.fastq.gz}

/home/gbucci/.local/bin/bbduk.sh in=$R1   in2=$R2   \
out=${name}_R1.duk.fq.gz out2=${name}_R2.duk.fq.gz \
ref=/lustre1/genomes/Illumina_Adapters/BBDUK/adapters.fa \
k=23 mink=11 rcomp=t ktrim=f kmask=X qtrim=rl trimq=5 \
forcetrimleft=3 forcetrimright2=3 overwrite=true

##for i in `ls x??`; do qsub -v arg1="$i" -N ${i:0:6} ~/PRJ/Scripts/BBDUK.sh ; done

