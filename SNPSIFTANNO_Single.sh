#PBS -l select=1:app=java:ncpus=4:mem=8gb
#PBS -P 161 
#PBS -V
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1
name=$arg2

cat $targ |java -jar /usr/local/cluster/src/snpEff/SnpSift.jar annotate /lustre1/genomes/hg19/annotation/Cosmic.hg19.vcf | java -jar /usr/local/cluster/src/snpEff/SnpSift.jar annotate /lustre1/genomes/hg19/annotation/dbSNP-142.vcf.gz  > ${targ%%.vcf}.Anno.vcf 




###for i in `ls Merged.chr*.vcf.gz`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/SNPSIFTANNO.sh ; done
