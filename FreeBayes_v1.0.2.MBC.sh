#PBS -l select=1:app=java:ncpus=4:mem=16gb
#PBS -P 160 
#PBS -V
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
chunk=$arg1
##isplit -l 120 -d Regions.bed

/lustre1/tools/bin/freebayes -f /lustre1/genomes/hg19/fa/hg19.fa \
-F 0.01 -C 2 --strict-vcf \
-p 6 --pooled-discrete --use-mapping-quality \
--min-base-quality 20 \
--min-mapping-quality 1 \
--genotype-qualities --report-genotype-likelihood-max \
-L BAMLIST.txt -r $chunk -v  ${chunk}.vcf


bgzip -f ${chunk}.vcf; tabix -fp vcf ${chunk}.vcf.gz

###for f in chr{1..22} chr{X,Y,M}; do qsub -v arg1="$f" -N ${i:0:6} ~/PBSScripts/FreeBayes.sh   ; done 
