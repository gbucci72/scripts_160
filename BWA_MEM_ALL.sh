#PBS -l select=1:app=java:ncpus=4:mem=6gb
#PBS -P 160 
#PBS -V
#PBS -q workq
#PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 

R1=$arg1
R2=$arg2
ref_genome=/lustre1/genomes/hg19/bwa/hg19
fasta_genome=/lustre1/genomes/hg19/fa/hg19.fa
reference=hg19
name=${R1%%_*}
#PBS -N $name
lane=1
echo $name
bwa mem -t 4 -w 1000 -a -M -R "@RG\tID:$name\tPL:illumina\tPU:$lane\tSM:$name\tLB:$name\tCN:CTGB\tSO:coordinate" \
$ref_genome $R1 $R2| samtools view -Su - | samtools sort - ${name}_${reference} 
samtools index ${name}_${reference}.bam
#mv ${name}_${reference}.bam* /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/160630_SN859_0309_AHJ2KVBCXX/BAM/AGILENT/
#ln -s /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/160630_SN859_0309_AHJ2KVBCXX/BAM/AGILENT/${name}_${reference}.bam .
