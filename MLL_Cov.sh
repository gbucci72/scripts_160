#exon 3 chr11:118342352-118345055
#exon 27 chr11: 118373088-118377386

bedtools intersect -wa -a /lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Amplicons.bed -b Exon3.bed  > Exon3_MLL.bed
bedtools intersect -wa -a /lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Amplicons.bed -b Exon27.bed  > Exon27_MLL.bed

#single patient
bedtools coverage -a Exon3_MLL.bed -b  /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/BAM/Trimmed_BBDUK/MAIL38_hg19.bam

for i in `ls /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/BAM/Trimmed_BBDUK/*bam| tee`; do A=`basename $i`; echo ${A%%_hg19.bam}; done > Names.txt

 ls /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/BAM/Trimmed_BBDUK/*bam| tee> Files.txt

 bedtools multicov -D -q 20 -bams `cat Files.txt`  -bed Exon3_MLL.bed  > Exon3_MLL.cov &

 bedtools multicov -D -q 20 -bams `cat Files.txt` -bed Exon27_MLL.bed  > Exon27_MLL.cov &
######################
#faccio su MBC Haloplex
cd /lustre2/scratch/Vago/160/Haloplex/160630_SN859_0309_AHJ2KVBCXX/FASTQ/BAM/MLL

for i in `ls /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/160630_SN859_0309_AHJ2KVBCXX/BAM/MBC/*bam| tee`; do A=`basename $i`; echo ${A%%_hg19_MBC.bam}; done > Names.txt
 ls /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/160630_SN859_0309_AHJ2KVBCXX/BAM/MBC/*bam| tee> Files.txt

########################
#faccio su HAloplex non MBC

for i in `ls /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/160630_SN859_0309_AHJ2KVBCXX/BAM/BBDUK/*bam| tee`; do A=`basename $i`; echo ${A%%_hg19.bam}; done > Names.txt

 ls /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/160630_SN859_0309_AHJ2KVBCXX/BAM/BBDUK/*bam| tee> Files.txt

 bedtools multicov -D -q 20 -bams `cat Files.txt`  -bed Exon3_MLL.bed  > Exon3_MLL.cov &

 bedtools multicov -D -q 20 -bams `cat Files.txt` -bed Exon27_MLL.bed  > Exon27_MLL.cov &
#R

