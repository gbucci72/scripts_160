#PBS -l select=1:app=java:ncpus=2:mem=4gb
#PBS -P 160 
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 

chunk=$arg1

condactivate

freebayes -f /lustre1/genomes/hg19/fa/hg19.fa \
-F 0.01 -C 2 -k -w -V -a -4 --strict-vcf \
--pooled-continuous  \
--min-base-quality 20 --min-mapping-quality 1 \
--haplotype-length 0 \
-v  ${chunk}.pre.vcf -t $chunk -L BAMLIST.txt


bgzip -f ${chunk}.pre.vcf; tabix -fp vcf ${chunk}.pre.vcf.gz

###
	
##find /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/160630_SN859_0309_AHJ2KVBCXX/BAM/BBDUK/CLIPPED/MARKED/ -name *bam > BAMLIST.txt
## wc BAMLIST.txt
##for i in `ls x??`;do qsub -v arg1="$i" -N M_${i:0:6} /lustre2/scratch/Vago/160/Haloplex/scripts_160/FreeBayes_v1.0.2.MBC.Multi.Naive.sh;done 
