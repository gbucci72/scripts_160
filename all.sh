#!/bin/bash

FQ1=$1
FQ2=$2
COVERED_HEAD=$3  #full path to the target capture regions, eg Manefest_TSO.txt
TARGET_HEAD=$COVERED_HEAD  

TOOL_DIR=./tool #create a directory hosting all tools 
ANNO_DIR=./anno #create a directory hosting all annotation libraries
REF=$ANNO_DIR/hg19/ucsc.hg19.normal.fasta
Anno_GATK=$ANNO_DIR/gatk2.5
PICARD=$TOOL_DIR/picard-tools-1.105
GATK=$TOOL_DIR/GenomeAnalysisTK-2.8-1-g932cd3a/GenomeAnalysisTK.jar
TRIM_GALORE=$TOOL_DIR/trim_galore
BWA=$TOOL_DIR/bwa-0.7.5a
SAMTOOLS=$TOOL_DIR/samtools-0.1.19
BAMUTIL=$TOOL_DIR/ngsutils/bin
VCFTOOLS=$TOOL_DIR/vcftools_0.1.10/bin/vcftools
ANNOVAR=$TOOL_DIR/annovar
JAVA=$TOOL_DIR/java/jre1.7.0_45/bin/java

COVERED_INT=$COVERED_HEAD.interval.txt
COVERED=$COVERED_HEAD.nohead.bed
TARGET_INT=$TARGET_HEAD.interval.txt
TARGET=$TARGET_HEAD.nohead.bed

SAMPLE1=$(basename $FQ1)
SAMPLE2=$(basename $FQ2)
SAMPLE1=${SAMPLE1/.fastq*}
SAMPLE2=${SAMPLE2/.fastq*}
SAMPLE=${SAMPLE1/_R1*}

OG=hg19
NT=4
RG="@RG\tID:$SAMPLE\tSM:$SAMPLE\tPL:illumina\tLB:$SAMPLE\tPU:$SAMPLE"
A1="AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC"
A2="AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT"
WD=$SAMPLE.$OG # Output directory.
BAM=$WD/$SAMPLE.recal_reads.bam

set -e

function echo_step()
{
    str=$1
    echo "####################"
    echo "# $str"
    echo "####################"
}

function collect_matrix()
{
	AS_Matrix=$1 # Picard AlignmentSummaryMetrics
	HS_Matrix=$2 # Picard HSMetrics

	if [ ! -s $AS_Matrix.summary ];then
		AS_array=( 2 6 )
		for i in "${AS_array[@]}"
		do
			sed -n '7p' $AS_Matrix |cut -f 1,$i >> $AS_Matrix.summary
			sed -n '8p' $AS_Matrix |cut -f 1,$i >> $AS_Matrix.summary
			sed -n '9p' $AS_Matrix |cut -f 1,$i >> $AS_Matrix.summary
			sed -n '10p' $AS_Matrix |cut -f 1,$i >> $AS_Matrix.summary
		done

		HS_array=( 2 3 4 5 13 14 17 21 22 28 29 30 31 )
		for i in "${HS_array[@]}"
		do
			sed -n '7,8p' $HS_Matrix | cut -f $i | awk '{printf("%s\t", $0)} END { printf( "\n" ); }' >> $AS_Matrix.summary
		done
		sed -i -s 's/\t$//' $AS_Matrix.summary
	fi	
}

if [ ! -d $WD ];then
    echo_step "Making Directory '$WD'"
    mkdir $WD
else
    echo_step "Directory '$WD' existes"
fi

# # Trim adaptors
if [ ! -s $WD/${SAMPLE2}_val_2.fq.gz ];then
	echo_step "Fastq trimming..."
	#echo "$TRIM_GALORE/trim_galore --paired -a $A1 -a2 $A2 -o $WD --length 20 $FQ1 $FQ2"
	$TRIM_GALORE/trim_galore --paired -a $A1 -a2 $A2 -o $WD --length 20 $FQ1 $FQ2
fi
# BWA align
if [ ! -s $WD/$SAMPLE.sam ];then
	echo_step "BWA mem..."
	$BWA/bwa mem -t $NT -M -R "$RG" $REF $WD/${SAMPLE1}_val_1.fq.gz $WD/${SAMPLE2}_val_2.fq.gz > $WD/$SAMPLE.sam
fi

# Convert to Bam
if [ ! -s $WD/$SAMPLE.bam ];then
    echo_step "Bam output..."
    $SAMTOOLS/samtools view -bSo $WD/$SAMPLE.bam $WD/$SAMPLE.sam
fi

# Sorting
if [ ! -s $WD/$SAMPLE.sorted.bam ];then
    echo_step "Sorting..."
    $SAMTOOLS/samtools sort -m 4G $WD/$SAMPLE.bam $WD/$SAMPLE.sorted
	$SAMTOOLS/samtools index $WD/$SAMPLE.sorted.bam
fi

tail -n +6 $COVERED_HEAD > $COVERED_HEAD.tmp
awk '{print $2"\t"$3"\t"$4}' $COVERED_HEAD.tmp  > $COVERED_HEAD.tmp1
awk '{print $1}' $COVERED_HEAD.tmp  > $COVERED_HEAD.tmp2
awk -F"." '{print $1}' $COVERED_HEAD.tmp2 > $COVERED_HEAD.tmp3
paste -d'\t' $COVERED_HEAD.tmp1 $COVERED_HEAD.tmp3 > $COVERED

echo -e "@HD\tVN:1.3\tSO:coordinate" > $COVERED_INT
head -n 500 $WD/$SAMPLE.sam | grep ^@ >> $COVERED_INT
awk '{print $1"\t"$2"\t"$3"\t""+""\t"$4}' $COVERED >> $COVERED_INT

# Picard stats
# if [ ! -s $WD/$SAMPLE.sorted.picard_HS_matrix ];then
	# echo_step "Sorted BAM Picard stats..."

	# $JAVA -jar -Xmx4G $PICARD/CollectAlignmentSummaryMetrics.jar R=$REF \
	# I=$WD/$SAMPLE.sorted.bam O=$WD/$SAMPLE.sorted.picard_AlignmentSummary_matrix \
	# MAX_INSERT_SIZE=1000 ASSUME_SORTED=true

	# $JAVA -jar -Xmx4G $PICARD/CalculateHsMetrics.jar R=$REF \
	# I=$WD/$SAMPLE.sorted.bam O=$WD/$SAMPLE.sorted.picard_HS_matrix \
	# BI=$COVERED_INT \
	# TI=$TARGET_INT \
	# PER_TARGET_COVERAGE=$WD/$SAMPLE.sorted.picard_HS_matrix_per_target
# fi

# Filter
if [ ! -s $WD/$SAMPLE.filtered.bam ];then
	echo_step "Filtering..."
	$BAMUTIL/bamutils filter $WD/$SAMPLE.sorted.bam $WD/$SAMPLE.filtered.bam -mapped
	$SAMTOOLS/samtools index $WD/$SAMPLE.filtered.bam
fi

# Picard stats
# if [ ! -s $WD/$SAMPLE.filtered.picard_HS_matrix ];then
    # echo_step "Filtered BAM Picard stats..."

    # $JAVA -jar -Xmx4G $PICARD/CollectAlignmentSummaryMetrics.jar R=$REF \
	# I=$WD/$SAMPLE.filtered.bam O=$WD/$SAMPLE.filtered.picard_AlignmentSummary_matrix \
    # MAX_INSERT_SIZE=1000 ASSUME_SORTED=true

    # $JAVA -jar -Xmx4G $PICARD/CalculateHsMetrics.jar R=$REF \
	# I=$WD/$SAMPLE.filtered.bam O=$WD/$SAMPLE.filtered.picard_HS_matrix \
    # BI=$COVERED_INT \
    # TI=$TARGET_INT \
	# PER_TARGET_COVERAGE=$WD/$SAMPLE.filtered.picard_HS_matrix_per_target
# fi

# Stats summary
# if [ ! -s $WD/$SAMPLE.filtered.picard_AlignmentSummary_matrix.summary ];then
	# echo_step "Stats summary..."
	# collect_matrix $WD/$SAMPLE.sorted.picard_AlignmentSummary_matrix $WD/$SAMPLE.sorted.picard_HS_matrix
	# collect_matrix $WD/$SAMPLE.filtered.picard_AlignmentSummary_matrix $WD/$SAMPLE.filtered.picard_HS_matrix
# fi

# Create realignment interval
if [ ! -s $WD/$SAMPLE.intervals ];then
	echo_step "Realigner Target Creator..."
	$JAVA -jar -Xmx4G $GATK \
	-T RealignerTargetCreator \
	-R $Anno_GATK/ucsc.hg19.fasta \
	-L $COVERED \
	-o $WD/$SAMPLE.intervals \
	-I $WD/$SAMPLE.filtered.bam \
	-known $Anno_GATK/1000G_phase1.indels.hg19.vcf \
	-known $Anno_GATK/Mills_and_1000G_gold_standard.indels.hg19.vcf \
	-nt $NT \
	-U ALL
fi

# Realignment
if [ ! -s $WD/$SAMPLE.Realigned.bam ];then
	echo_step "Indel Realigner..."
	$JAVA -Xmx4G -jar $GATK \
	-I $WD/$SAMPLE.filtered.bam \
	-R $Anno_GATK/ucsc.hg19.fasta \
	-T IndelRealigner \
	-targetIntervals $WD/$SAMPLE.intervals \
	-o $WD/$SAMPLE.Realigned.bam \
	-known $Anno_GATK/1000G_phase1.indels.hg19.vcf \
	-known $Anno_GATK/Mills_and_1000G_gold_standard.indels.hg19.vcf \
	-U ALL
fi

# Calculate covariant
if [ ! -s $WD/$SAMPLE.recal_data.table ];then
	echo_step "Calculate recal data table..."
	$JAVA -Xmx4G -jar $GATK \
	-I $WD/$SAMPLE.Realigned.bam \
	-R $Anno_GATK/ucsc.hg19.fasta \
	-T BaseRecalibrator \
	-L $COVERED \
	-o $WD/$SAMPLE.recal_data.table \
	-knownSites $Anno_GATK/dbsnp_138.hg19.vcf \
	-knownSites $Anno_GATK/1000G_phase1.indels.hg19.vcf \
	-knownSites $Anno_GATK/Mills_and_1000G_gold_standard.indels.hg19.vcf \
	-nct $NT \
	-U ALL
fi

# Calculate covariant
if [ ! -s $WD/$SAMPLE.recal_reads.bam ];then
	echo_step "Apply Recalibration..."
	$JAVA -Xmx4G -jar $GATK \
	-I $WD/$SAMPLE.Realigned.bam \
	-R $Anno_GATK/ucsc.hg19.fasta \
	-T PrintReads \
	-L $COVERED \
	-BQSR $WD/$SAMPLE.recal_data.table \
	-o $WD/$SAMPLE.recal_reads.bam \
	-nct $NT \
	-U ALL
fi

# generate vcf results
if [ ! -s $BAM.raw_SNVs_UG.vcf ]; then
        echo_step "Unified Genotyper..."
        $JAVA -jar -Xmx4G $GATK \
        -T UnifiedGenotyper -R $REF \
        -I $BAM \
        -L $COVERED \
		--dbsnp $Anno_GATK/dbsnp_138.hg19.vcf \
        -o $BAM.raw_SNVs_UG.vcf \
        -dt BY_SAMPLE \
        --downsample_to_coverage 5000 \
        --genotype_likelihoods_model BOTH \
        --standard_min_confidence_threshold_for_calling 10 \
        --standard_min_confidence_threshold_for_emitting 10 \
        --max_alternate_alleles 3 \
        -nt $NT \
        --annotation BaseQualityRankSumTest \
        --annotation Coverage \
        --annotation DepthPerAlleleBySample \
        --annotation FisherStrand \
        --annotation HaplotypeScore \
        --annotation HomopolymerRun \
        --annotation MappingQualityRankSumTest \
        --annotation QualByDepth \
        --annotation ReadPosRankSumTest
fi

# if [ ! -s $BAM.filtered_SNVs_UG.vcf ]; then
	# echo_step "Filter SNVs..."
	# $JAVA -jar -Xmx4G $GATK \
	# -T VariantFiltration -R $REF \
	# -V $BAM.raw_SNVs_UG.vcf \
	# --filterExpression "DP <= 10.0" \
	# --filterExpression "QD < 2.0" \
	# --filterExpression "FS > 35.0" \
	# --filterExpression "MQ < 40.0" \
	# --filterExpression "HaplotypeScore > 45.0" \
	# --filterExpression "MQRankSum < -12.5" \
	# --filterExpression "ReadPosRankSum < -8.0" \
	# --filterName "DPFilter" \
	# --filterName "QDFilter" \
	# --filterName "FSFilter" \
	# --filterName "MQFilter" \
	# --filterName "HSFilter" \
	# --filterName "MQRankFilter" \
	# --filterName "ReadPosFilter" \
	# -o $BAM.filtered_SNVs_UG.vcf 
# fi

if [ ! -s $BAM.raw_SNVs_UG_onTarget.vcf ]; then
	echo_step "Calculate on Target SNVs..."
	$VCFTOOLS --vcf $BAM.raw_SNVs_UG.vcf --bed $TARGET --recode-INFO-all --recode --out $BAM.raw_SNVs_UG_onTarget
	mv $BAM.raw_SNVs_UG_onTarget.recode.vcf $BAM.raw_SNVs_UG_onTarget.vcf
fi

# if [ ! -s $BAM.filtered_SNVs_UG_offTarget.vcf ]; then
	# echo_step "Calculate off Target SNVs..."
	# $VCFTOOLS --vcf $BAM.raw_SNVs_UG.vcf --exclude-bed $TARGET --recode-INFO-all --recode --out $BAM.filtered_SNVs_UG_offTarget
	# if [ -s $BAM.filtered_SNVs_UG_offTarget.vcf ]; then
		# mv $BAM.filtered_SNVs_UG_offTarget.recode.vcf $BAM.filtered_SNVs_UG_offTarget.vcf
	# fi
# fi

# function vcf_stats()
# {
	# VCF=$1
	# STATS=$2
	# if [ ! -s $STATS ]; then
		# echo_step "Calculate filtered SNVs stats..."
		# printf "Total_SNVs\t" > $STATS
		# grep -v "#" $VCF| wc -l >> $STATS
		# printf "DP<=10\t" >> $STATS
		# grep -v "#" $VCF|grep -v -P "DPFilter" | wc -l >> $STATS
		# printf "LowQual<30\t" >> $STATS
		# grep -v "#" $VCF|grep -v -P "DPFilter|LowQual" | wc -l >> $STATS
		# printf "QD<2\t" >> $STATS
		# grep -v "#" $VCF|grep -v -P "DPFilter|LowQual|QDFilter" | wc -l >> $STATS
		# printf "FS>35\t" >> $STATS
		# grep -v "#" $VCF|grep -v -P "DPFilter|LowQual|QDFilter|FSFilter" | wc -l >> $STATS
		# printf "MQ<40\t" >> $STATS
		# grep -v "#" $VCF|grep -v -P "DPFilter|LowQual|QDFilter|FSFilter|MQFilter" | wc -l >> $STATS
		# printf "HaplotypeScore>45\t" >> $STATS
		# grep -v "#" $VCF|grep -v -P "DPFilter|LowQual|QDFilter|FSFilter|MQFilter|HSFilter" | wc -l >> $STATS
		# printf "MQRankSum<-12.5\t" >> $STATS
		# grep -v "#" $VCF|grep -v -P "DPFilter|LowQual|QDFilter|FSFilter|MQFilter|HSFilter|MQRankFilter" | wc -l >> $STATS
		# printf "ReadPosRankSum<-8\t" >> $STATS
		# grep -v "#" $VCF|grep -v -P "DPFilter|LowQual|QDFilter|FSFilter|MQFilter|HSFilter|MQRankFilter|ReadPosFilter" | wc -l >> $STATS
	# fi
# }
# vcf_stats $BAM.raw_SNVs_UG.vcf $BAM.raw_SNVs_UG.stats
# vcf_stats $BAM.filtered_SNVs_UG_onTarget.vcf $BAM.filtered_SNVs_UG_onTarget.stats

# run annovar annotation
if [ ! -s $BAM.raw_SNVs_UG.hg19_multianno.txt ]; then
	echo_step "Calculate hg19_multianno..."
	$ANNOVAR/convert2annovar.pl -format vcf4 $BAM.raw_SNVs_UG.vcf  -outfile $BAM.raw_SNVs_UG.avinput

	$ANNOVAR/table_annovar.pl $BAM.raw_SNVs_UG.avinput $TOOL_DIR/annovar/humandb/ -buildver hg19 -outfile $BAM.raw_SNVs_UG -remove -protocol snp138,refGene,phastConsElements46way,genomicSuperDups,esp6500si_all,esp6500si_ea,esp6500si_aa,1000g2012apr_all,1000g2012apr_eur,1000g2012apr_amr,ljb2_all -operation f,g,r,r,f,f,f,f,f,f,f -nastring NA
fi

# run annotation summary script
if [ ! -s  $WD.txt ]; then
	echo_step "Generate final result file ..."
	perl report.pl $WD $SAMPLE.recal_reads.bam.raw_SNVs_UG.hg19_multianno.txt
fi
# save results into database
#perl record.pl $WD.txt




