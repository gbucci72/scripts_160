#!/usr/bin/env python

from operator import itemgetter
import sys, argparse, pysam


__author__ = "Gabriele Bucci (bucci.g@gmail.com)"
__version__ = "$Revision: 0.0.1 $"
__date__ = "$Date: 2016-02-12 11:31 $"

parser = argparse.ArgumentParser()
parser.add_argument("bamfile",help="the indexed bam file",type=file)
args = parser.parse_args()

in_bam = pysam.Samfile(args.bamfile, 'rb')
for read in in_bam.fetch('chr1', 100, 120):
     print read
in_bam.close()
