#PBS -l select=1:app=java:ncpus=4:mem=8gb
#PBS -P 161 
#PBS -V
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
chunk=$arg1
##isplit -l 120 -d Regions.bed

/lustre1/tools/bin/freebayes -f /lustre1/genomes/hg19/fa/hg19.fa \
-F 0.01 -C 10 -4 -j --strict-vcf \
--pooled-continuous --use-mapping-quality \
--min-base-quality 20 \
--min-mapping-quality 1 \
--genotype-qualities --report-genotype-likelihood-max \
-L BAMLIST.txt -t $chunk -v  ${chunk}.vcf


bgzip -f ${chunk}.vcf; tabix -fp vcf ${chunk}.vcf.gz

###for i in `ls x??`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/FreeBayes.sh ; done
