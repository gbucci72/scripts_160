#PBS -l select=1:app=java:ncpus=1:mem=2gb
#PBS -P 160
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1
name=$arg2

condactivate

vcfkeepsamples $targ $name | vcfbreakmulti|bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa|vcfsort|vcfuniq|vcffixup - |snpSift filter "GEN[0].AO[ANY] >= 1" |vcfsort|vcfuniq|vcffixup - | bgzip -f -c > ${name}.${targ}   




###for i in `ls Merged.chr*.vcf.gz`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/SNPSIFTANNO.sh ; done
