#MAF PRE E POST MBC


cd /lustre2/scratch/Vago/160/Haloplex/161102_SN859_0372_AHWF5JBCXX/FASTQ/BBDUK/BAM/CLIPPED_NM7/MARKED/CHUNKS/BYSAMPLE/MAF

snpSift filter "(GEN[0].AO[0] > 0 )" DEIV163.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2ACHL.vcf.gz | \
snpSift extractFields - "CHROM" "POS" "REF" "ALT" "ANN[0].GENE" "GEN[0].RO" "GEN[0].AO[0]" "PREMBC"| \
awk '{OFS="\t"; print $1"_"$2"_"$3"_"$4"_"$5"_"$6"_"$7"_"$8}' | \
awk 'FS="_" {OFS="\t"; print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' |\
awk 'NR > 1 {OFS="\t"; print $1,$2,$3,$4,$5,($7/($7+$6)),($10/($10+$9))}' > DEIV163.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2ACHL.AF.txt


snpSift filter "(GEN[0].AO[0] > 0 )" DEIV163.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2ACHL.vcf.gz | \
snpSift extractFields - "CHROM" "POS" "REF" "ALT" "ANN[0].GENE" "GEN[0].RO" "GEN[0].AO[0]" "PREMBC"| \
awk '{OFS="\t"; print $1"_"$2"_"$3"_"$4"_"$5"_"$6"_"$7"_"$8}' | \
awk 'FS="_" {OFS="\t"; print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' |\
awk 'NR > 1 {OFS="\t"; print $1,$2,$3,$4,$5,($7/($7+$6)),($10/($10+$9)),$7+$6,$8}' > DEIV163.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2ACHL.AF.txt

#R

dir=c("/lustre2/scratch/Vago/160/Haloplex/161102_SN859_0372_AHWF5JBCXX/FASTQ/BBDUK/BAM/CLIPPED_NM7/MARKED/CHUNKS/BYSAMPLE/MAF")
name=c("DEIV163.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2ACHL.AF.txt")
File=file.path(dir,name)
AFT<-read.table(File)
head(AFT)



plot(log2(AFT$V7) ~ log2(AFT$V6),xlab="PRE MBC",ylab="POST MBC",pch=16,cex=0.6,col="red")

points(x=log2(AFT[which(abs(log2(AFT$V7)-log2(AFT$V6)) >= 1 ),6]),y=log2(AFT[which(abs(log2(AFT$V7)-log2(AFT$V6)) >= 1 ),7]),pch=16,col="green")

plot(log2(AFT$V7) ~ log2(AFT$V6),xlab="PRE MBC",ylab="POST MBC",pch=16,cex=0.6,col="red")
points(x=log2(AFT[which(abs(log2(AFT$V7)-log2(AFT$V6)) >= 1 ),6]),y=log2(AFT[which(abs(log2(AFT$V7)-log2(AFT$V6)) >= 1 ),7]),pch=16,col="green")
    
    