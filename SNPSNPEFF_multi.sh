#PBS -l select=1:app=java:ncpus=1:mem=16gb
#PBS -P 161 
#PBS -V
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1

condactivate

zcat $targ |vcfbreakmulti | bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa|vcfentropy -f /lustre1/genomes/hg19/fa/hg19.fa -w 50 | snpEff -v -noDownload -noStats -lof -t GRCh37.75 | vcfsort| bgzip -f -c > ${targ%%.vcf.gz}.Eff.vcf.gz

tabix ${targ%%.vcf.gz}.Eff.vcf.gz 



###for i in `ls Merged.chr*.vcf.gz`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/SNPSIFTANNO.sh ; done
