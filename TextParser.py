#!/usr/bin/env python

from operator import itemgetter
import sys, argparse, pysam

parser = argparse.ArgumentParser()
parser.add_argument("bam",help="the indexed bam file",type=file)
args = parser.parse_args()
print args.bam

for line in args.bam:
	# remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    words = line.split()
    #print '%s%s%s' (words[1],'_',words[0])
    BC = (words[1]+"_"+words[0])
    print '%s' % BC


