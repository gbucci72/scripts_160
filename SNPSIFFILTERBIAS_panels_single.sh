#PBS -l select=1:app=java:ncpus=4:mem=16gb
#PBS -P 160 
#PBS -V
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1
name=$arg2


zcat $targ |vcfbreakmulti| vcfsort| java -jar /usr/local/cluster/src/snpEff/SnpSift.jar filter \
"( DP > 100 ) & ( MQM[ANY] > 20 & MQMR[ANY] > 20 ) &  \
((RPL[ANY] > 1) | (RPR[ANY] > 1)) & (SAF[ANY] > 0) & (SAR[ANY] >0)"  | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar filter \
"(( GEN[ANY].DP[ANY] > 100 ) & \
  (GEN[ANY].AO[ANY] > 5) & ( GEN[ANY].QA[ANY] > 30 ))" | \
bgzip -c > ${targ%%.vcf.gz}.multi.unBias.Qual.vcf.gz

tabix ${targ%%.vcf.gz}.multi.unBias.Qual.vcf.gz



###for i in `ls Merged.chr*.vcf.gz`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/SNPSIFTANNO.sh ; done
