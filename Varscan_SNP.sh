#!/bin/bash
#${NAME}3.merge.dedup.bam
FILE1=$2
#${NAME}1.merge.dedup.bam
NAME=${FILE1/_*/}
cat <<__EOF__> ${NAME}.txt
$NAME
__EOF__

#/lustre1/genomes/hg19/fa/hg19.fa
GENOME=$1
currentDirectory=`pwd`
script_name=${NAME}.sh

cat <<__EOF__> $script_name

#!/bin/bash
#PBS -N $script_name
#PBS -l select=1:ncpus=4:mem=6g:app=java
#PBS -V
#PBS -Wsandbox=private
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it
#PBS -m ae

echo "copy number $FILE1 on $GENOME"
##samtools mpileup -q 0 -B -f  $GENOME ${currentDirectory}/$FILE1 | java -jar /home/gbucci/.local/bin/VarScan.v2.3.9.jar mpileup2cns --min-var-freq 0.01 --output-vcf > ${currentDirectory}/${FILE1%%.bam}.vcf 
samtools mpileup -q 0 -B -f  $GENOME ${currentDirectory}/$FILE1 |  java -jar /home/gbucci/.local/bin/VarScan.v2.4.1.jar mpileup2cns --min-coverage 3 --min-var-freq 0.01 --p-value 0.10 --somatic-p-value 0.05 --strand-filter 0 --output-vcf 1 --variants --vcf-sample-list ${NAME}.txt |bgzip -c > ${currentDirectory}/${FILE1%%.bam}.vcf.gz

tabix -f ${currentDirectory}/${FILE1%%.bam}.vcf.gz 


__EOF__

qsub $script_name

