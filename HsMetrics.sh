#!/bin/bash

R1=$1	#BAM
dict=$2
baits=$3
target=$4
name=${R1%%_*}

user=`whoami`

 
BWA=/usr/local/cluster/bin/bwa
SAM=/usr/local/cluster/bin/samtools
script_name=${name}_HS


cat <<__EOF__> ${script_name}.sh
HOST=`hostname`

#PBS -N $script_name
#PBS -l select=1:ncpus=2:mem=4g:app=java
#PBS -V
#PBS -o ${name}_${HOST}.log
#PBS -e ${name}_${HOST}.err
#PBS -q workq
#PBS -M bucci.gabriele@hsr.it
#PBS -m ae
#PBS -Wsandbox=private

cd $PWD
#HOST=`hostname`
ulimit -l unlimited
ulimit -s unlimited
echo $HOST

java -jar /usr/local/cluster/bin/picard.jar CalculateHsMetrics \
    BAIT_SET_NAME="HaloplexHS-25069-1432135121" \
    BI=$baits \
    TI=$target \
    I=$R1 \
    O=${name}.hsmetrics \
    REFERENCE_SEQUENCE=$dict \
    PER_TARGET_COVERAGE=${name}_TARGET_COVERAGE.txt \
    VALIDATION_STRINGENCY=SILENT

__EOF__

qsub ${script_name}.sh
