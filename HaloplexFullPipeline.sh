#Variant Calling Pipeline Haloplex HS
#the fastq  including I2 index
#Define directories and names
PRJ=/lustre2/scratch/Vago/160/Haloplex/161102_SN859_0372_AHWF5JBCXX
FASTQDIR=/lustre2/raw_data/161102_SN859_0372_AHWF5JBCXX/Ciceri_160_Relapsing_Leukemia/
BAM=/lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/Test/BAM #change this
COVERED=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Covered.bed
BI=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Amplicons.intervals
TI=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Regions.intervals
dict=/lustre1/genomes/hg19/fa/hg19.fa

##########################################################################################################################
##
if [[ ! -d $PRJ ]]
	then
echo "no such dir" $PRJ
	else echo "directory exists"
fi
if [[ ! -d $FASTQDIR ]]
	then
echo "no such dir" $FASTQDIR
	else echo "directory exists"
fi
##########################################################################################################################
#####
#NAMES
cd $FASTQDIR
##########################################################################################################################
##########################################################################################################################
#RENAME
#remove illumina index file
#rm *_I1_001.fastq.gz
mkdir Renamed
cd Renamed

for i in `find $FASTQDIR -maxdepth 1 -name "*_R1_*"`
do A=`basename $i`
ln -s $i ${A}
done

for i in `find  $FASTQDIR -maxdepth 1 -name "*_R2_*"`
do A=`basename $i`
ln -s $i ${A/_R2_/_I1_}
done

for i in `find  $FASTQDIR -maxdepth 1 -name "*_R3_*"`
do A=`basename $i`
ln -s $i ${A/_R3_/_R2_}
done
##########################################################################################################################
#TEST
mkdir Test
for i in `ls F*fastq.gz`; do zcat $i | head -n 5000 | gzip -c > Test/$i; done 
cd Test
FASTQDIR=${FASTQDIR}Renamed/Test
##########################################################################################################################
#NAMES
for i in `ls *fastq.gz`; do echo $i; done| cut -d_ -f 1| sort -u > Names.txt
wc -l Names.txt
##########################################################################################################################
#login1
#BBDUK
for i in `ls *_R1_*fastq.gz`; do qsub -v arg1="$i" -N ${i:0:6} /lustre2/scratch/Vago/160/Haloplex/scripts_160/BBDUK.sh; done

mkdir BBDUK
mv *duk.fq.gz BBDUK
cd BBDUK
find . -type f -size +5k -name "*duk.fq.gz" -exec basename {} \;| cut -d_ -f 1| sort -u > Names.txt
diff -a -q ../Names.txt Names.txt

##########################################################################################################################
#ALIGN
grep ^chr $COVERED > Regions.bed #not used here

for i in `ls *R1.duk.fq.gz`; do /lustre2/scratch/Vago/160/Haloplex/scripts_160/BWABBDUK.lane.sh $i hg19 ; done

mkdir BAM
mv *.bam* BAM
cd BAM
find . -type f -size +5k -name "*.bam" -exec basename {} \;| cut -d_ -f 1| sort -u > Names.txt
diff -a -q ../Names.txt Names.txt
##########################################################################################################################
#CLIPPED NM7
for i in `ls *hg19.bam`; do qsub -v arg1="$i" -N 1${i:0:5} /lustre2/scratch/Vago/160/Haloplex/scripts_160/BAM_Filter.sh; done

mkdir CLIPPED_NM7
mv *filtered*bam* CLIPPED_NM7
cd CLIPPED_NM7
find . -type f -size +5k -name "*filtered_clipped.bam" -exec basename {} \;| cut -d_ -f 1| sort -u > Names.txt
diff -a -q ../Names.txt Names.txt
#add test here before removing
rm *filtered.bam*
##########################################################################################################################
###HSMETRICS
for i in `ls *bam`; do /lustre2/scratch/Vago/160/Haloplex/scripts_160/HsMetrics.sh $i $dict $BI $TI $i; done
	
mkdir HSMETRICS
mv *hsmetrics HSMETRICS
pushd HSMETRICS

	readarray A < <(cat *.hsmetrics| grep -v "#"| grep BAIT|uniq| sed 's/\s/\n/g' )

	echo ${A[@]:0:43} ${A[@]:43:1}  > hsmetrics.tsv

	for i in `ls *.hsmetrics`;
	do
		 name=${i%%.hsmetrics}
		 readarray a < <(cat $i| grep -v "#" $i| grep -v BAIT|sed 's/\t/\n/g')
		 #echo ${!a[@]}
		 a[43]=$name
		 echo ${a[@]:0:43} ${a[@]:43:1}
	done >> hsmetrics.tsv

	sed  -i 's/ \+/\t/g' hsmetrics.tsv
popd
	
mkdir TARGET_COVERAGE
mv *TARGET_COVERAGE.txt TARGET_COVERAGE
pushd TARGET_COVERAGE

	for i in `ls *_TARGET_COVERAGE.txt`; do cat $i | cut -f 8 > ${i%%_TARGET_COVERAGE.txt}.norm.txt ; done
	ls *.norm.txt| tee| sed 's/.norm.txt//' > names.txt
	some=`ls  *_TARGET_COVERAGE.txt| head -n 1`
	paste <(cat $some | cut -f 1,2,3) <(cat $some | cut -f 5) *.norm.txt > Normalized_TARGETCOVERAGE.txt
	ls *.norm.txt| tee| sed 's/.norm.txt//' > names.txt
	for i in `ls *_TARGET_COVERAGE.txt`; do cat $i | cut -f 7 > ${i%%_TARGET_COVERAGE.txt}.raw.txt ; done
	paste <(cat $some | cut -f 1,2,3) <(cat $some | cut -f 5) *.raw.txt > Raw_TARGETCOVERAGE.txt
	for i in `ls *_TARGET_COVERAGE.txt`; do cat $i | cut -f 6 > ${i%%_TARGET_COVERAGE.txt}.GC.txt ; done
	paste <(cat $some | cut -f 1,2,3) <(cat $some | cut -f 5) *.GC.txt > GC_TARGETCONTENT.txt
popd
##########################################################################################################################
####MARK DUPLICATES
ln $FASTQDIR/*_I1_001.fastq.gz .

for i in `ls *filtered_clipped.bam`;do Index=${i%%_*}*_I1_*.fastq.gz; qsub -v arg1="$i",arg2="$Index" -N BC_${i:0:6} /lustre2/scratch/Vago/160/Haloplex/scripts_160/Agilent_Locatit.sh;done

for i in `ls *_sorted.bam*`;
     do echo $i
     mkdir MARKED
     mkdir -p ${BAM}/MARKED
     mv $i ${BAM}/MARKED
     ln -s ${BAM}/MARKED/${i} ./MARKED/
done

#check the bam




	
	
	
	
	
