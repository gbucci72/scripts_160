#Variant Calling Pipeline Haloplex HS
#the fastq  including I2 index
#Define directories and names
PRJ=/lustre2/scratch/Vago/160/Haloplex/170717_SN859_0458_AHM55WBCXY
RUN=170717_SN859_0458_AHM55WBCXY
FASTQDIR=/lustre2/raw_data/170717_SN859_0458_AHM55WBCXY/Ciceri_160_Realpsing_Leukemia/
BAM=/lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/Test/BAM #change this
COVERED=/lustre2/scratch/gbucci/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Covered.bed
BI=/lustre2/scratch/gbucci/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Amplicons.interval_list
TI=/lustre2/scratch/gbucci/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Regions.interval_list
dict=/lustre1/genomes/hg19/fa/hg19.fa

##########################################################################################################################
##
if [[ ! -d $FASTQDIR ]]
	then
echo "no such dir" $FASTQDIR
	else echo "directory exists"
fi

if [[ ! -d $PRJ ]]
	then
echo "no such dir" $PRJ; echo "creating " $PRJ; mkdir $PRJ
	else echo "directory exists"
fi

##########################################################################################################################
#####
#NAMES
cd $FASTQDIR
##########################################################################################################################
##########################################################################################################################
#RENAME
#remove illumina index file
#rm *_I1_001.fastq.gz
mkdir Renamed
cd Renamed

for i in `find $FASTQDIR -maxdepth 1 -name "*_R1_*"`
do A=`basename $i`
ln -s $i ${A}
done

for i in `find  $FASTQDIR -maxdepth 1 -name "*_R2_*"`
do A=`basename $i`
ln -s $i ${A/_R2_/_I2_}
done

for i in `find  $FASTQDIR -maxdepth 1 -name "*_R3_*"`
do A=`basename $i`
ln -s $i ${A/_R3_/_R2_}
done
##########################################################################################################################
FASTQDIR=${FASTQDIR}Renamed/Test
##########################################################################################################################
#NAMES
for i in `ls *fastq.gz|grep -v Cut`; do echo $i; done| cut -d_ -f 1| sort -u > Names.txt
wc -l Names.txt
##########################################################################################################################
#login1
#BBDUK
cd $FASTQDIR
for i in `ls *_R1_*fastq.gz|grep -v Cut`; do qsub -v arg1="$i" -N ${i:0:6} /lustre2/scratch/Vago/160/Haloplex/scripts_160/BBDUK.sh; done

mkdir BBDUK
mv *duk.fq.gz BBDUK
cd BBDUK
find . -type f -size +5k -name "*duk.fq.gz" -exec basename {} \;| cut -d_ -f 1| sort -u > Names.txt
diff -a -q ../Names.txt Names.txt

##########################################################################################################################
#ALIGN
grep ^chr $COVERED > Regions.bed #not used here
#b8ef586..1f3b454
for i in `ls *R1.duk.fq.gz`; do /lustre2/scratch/Vago/160/Haloplex/scripts_160/BWABBDUK.lane.sh $i hg19 ; done

mkdir BAM
mv *.bam* BAM
cd BAM
find . -type f -size +5k -name "*.bam" -exec basename {} \;| cut -d_ -f 1| sort -u > Names.txt
diff -a -q ../Names.txt Names.txt
##########################################################################################################################
#CLIPPED NM7#in the future dont use clipping bam but use appropriate freebayes filters on alignments gaps
for i in `ls *hg19.bam`; do qsub -v arg1="$i" -N 1${i:0:5} /lustre2/scratch/Vago/160/Haloplex/scripts_160/BAM_Filter.sh; done

mkdir CLIPPED_NM7
mv *filtered*bam* CLIPPED_NM7
cd CLIPPED_NM7
find . -type f -size +5k -name "*filtered_clipped.bam" -exec basename {} \;| cut -d_ -f 1| sort -u > Names.txt
diff -a -q ../Names.txt Names.txt
#add test here before removing
rm *filtered.bam*
##########################################################################################################################
###HSMETRICS
for i in `ls *bam`; do /lustre2/scratch/gbucci/Vago/160/Haloplex/scripts_160/HsMetrics.sh $i $dict $BI $TI $i; done
	
mkdir HSMETRICS
mv *hsmetrics HSMETRICS
pushd HSMETRICS

	readarray A < <(cat *.hsmetrics| grep -v "#"| grep BAIT|uniq| sed 's/\s/\n/g' )

	echo ${A[@]:0:43} ${A[@]:43:1}  > hsmetrics.tsv

	for i in `ls *.hsmetrics`;
	do
		 name=${i%%.hsmetrics}
		 readarray a < <(cat $i| grep -v "#" $i| grep -v BAIT|sed 's/\t/\n/g')
		 #echo ${!a[@]}
		 a[43]=$name
		 echo ${a[@]:0:43} ${a[@]:43:1}
	done >> hsmetrics.tsv

	sed  -i 's/ \+/\t/g' hsmetrics.tsv
popd
	
mkdir TARGET_COVERAGE

mv *TARGET_COVERAGE.txt TARGET_COVERAGE
pushd TARGET_COVERAGE

	for i in `ls *_TARGET_COVERAGE.txt`; do cat $i | cut -f 8 > ${i%%_TARGET_COVERAGE.txt}.norm.txt ; done
	ls *.norm.txt| tee| sed 's/.norm.txt//' > names.txt
	some=`ls  *_TARGET_COVERAGE.txt| head -n 1`
	paste <(cat $some | cut -f 1,2,3) <(cat $some | cut -f 5) *.norm.txt > Normalized_TARGETCOVERAGE.txt
	ls *.norm.txt| tee| sed 's/.norm.txt//' > names.txt
	for i in `ls *_TARGET_COVERAGE.txt`; do cat $i | cut -f 7 > ${i%%_TARGET_COVERAGE.txt}.raw.txt ; done
	paste <(cat $some | cut -f 1,2,3) <(cat $some | cut -f 5) *.raw.txt > Raw_TARGETCOVERAGE.txt
	for i in `ls *_TARGET_COVERAGE.txt`; do cat $i | cut -f 6 > ${i%%_TARGET_COVERAGE.txt}.GC.txt ; done
	paste <(cat $some | cut -f 1,2,3) <(cat $some | cut -f 5) *.GC.txt > GC_TARGETCONTENT.txt
popd
##########################################################################################################################
#UNMARKED
DIR=/lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/${RUN}/Ciceri_160_Realpsing_Leukemia/Renamed/BBDUK/BAM/CLIPPED_NM7/
for i in `ls *.bam*`; do mv $i $DIR; ln -s ${DIR}${i} . ; echo $i;done &
 
####MARK DUPLICATES
ln $FASTQDIR/*_I2_001.fastq.gz .

for i in `ls *filtered_clipped.bam`;do Index=${i%%_*}*_I2_*.fastq.gz; qsub -v arg1="$i",arg2="$Index" -N BC_${i:0:6} /lustre2/scratch/Vago/160/Haloplex/scripts_160/Agilent_Locatit.sh;done

for i in `ls *_sorted.bam*`;
     do echo $i
     mkdir MARKED
     #mkdir -p ${BAM}/MARKED
     #mv $i ${BAM}/MARKED
     mv $i MARKED
     #ln -s ${BAM}/MARKED/${i} ./MARKED/
done
###############
cd MARKED
find . -type f -size +5k -name "*.bam" -exec basename {} \;| cut -d_ -f 1| sort -u > Names.txt
diff -a -q ../Names.txt Names.txt
##########################################################################################################################
###HSMETRICS
for i in `ls *bam`; do /lustre2/scratch/Vago/160/Haloplex/scripts_160/HsMetrics.sh $i $dict $BI $TI $i; done
	
mkdir HSMETRICS
mv *hsmetrics HSMETRICS
pushd HSMETRICS

	readarray A < <(cat *.hsmetrics| grep -v "#"| grep BAIT|uniq| sed 's/\s/\n/g' )

	echo ${A[@]:0:43} ${A[@]:43:1}  > hsmetrics.tsv

	for i in `ls *.hsmetrics`;
	do
		 name=${i%%.hsmetrics}
		 readarray a < <(cat $i| grep -v "#" $i| grep -v BAIT|sed 's/\t/\n/g')
		 #echo ${!a[@]}
		 a[43]=$name
		 echo ${a[@]:0:43} ${a[@]:43:1}
	done >> hsmetrics.tsv

	sed  -i 's/ \+/\t/g' hsmetrics.tsv
popd
	
mkdir TARGET_COVERAGE

mv *TARGET_COVERAGE.txt TARGET_COVERAGE
pushd TARGET_COVERAGE

	for i in `ls *_TARGET_COVERAGE.txt`; do cat $i | cut -f 8 > ${i%%_TARGET_COVERAGE.txt}.norm.txt ; done
	ls *.norm.txt| tee| sed 's/.norm.txt//' > names.txt
	some=`ls  *_TARGET_COVERAGE.txt| head -n 1`
	paste <(cat $some | cut -f 1,2,3) <(cat $some | cut -f 5) *.norm.txt > Normalized_TARGETCOVERAGE.txt
	ls *.norm.txt| tee| sed 's/.norm.txt//' > names.txt
	for i in `ls *_TARGET_COVERAGE.txt`; do cat $i | cut -f 7 > ${i%%_TARGET_COVERAGE.txt}.raw.txt ; done
	paste <(cat $some | cut -f 1,2,3) <(cat $some | cut -f 5) *.raw.txt > Raw_TARGETCOVERAGE.txt
	for i in `ls *_TARGET_COVERAGE.txt`; do cat $i | cut -f 6 > ${i%%_TARGET_COVERAGE.txt}.GC.txt ; done
	paste <(cat $some | cut -f 1,2,3) <(cat $some | cut -f 5) *.GC.txt > GC_TARGETCONTENT.txt
popd
##################################################################################################################
########BARCODES

for i in `ls *sorted.bam`
do
    samtools view -f 1024 $i | grep  -o "XM\:Z\:[A-Z]\{10\}" | cut -d: -f 3| sort | uniq -c | sort -k1nr  > ${i%%.bam}_AM.txt
done &

#471e0bc..730a33
for names in `ls *sorted.bam`
do
R --vanilla --slave --args $names < /lustre2/scratch/Vago/160/Haloplex/scripts_160/BARCODE_Frequency.R
done &

##########################VARIANT CALL##################################################################
SPOSTA I BAM IN LUSTRE1 e CAMBIA PATHS3
#UNMARKED
DIR=/lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/${RUN}/Ciceri_160_Realpsing_Leukemia/Renamed/BBDUK/BAM/CLIPPED_NM7/MARKED/
for i in `ls *.bam*`; do mv $i $DIR; ln -s ${DIR}${i} . ; echo $i;done &
 
mkdir CHUNKS
cd CHUNKS
ln -s /lustre2/scratch/Vago/160/Haloplex/161102_SN859_0372_AHWF5JBCXX/FASTQ/BBDUK/BAM/CLIPPED_NM7/MARKED/CHUNKS/x?? .
cd ..

ls *bam| sort > BAMLIST.txt
#FACCIO SOLO I NUOVI
cat Nuovi.txt |while read i ; do ls ${i}*.bam; done| sort > CHUNKS/BAMLIST.txt
cd CHUNKS
cat ../Nuovi.txt |while read i ; do ln -s ../${i}*.bam* .; done

for i in `ls x??`;do qsub -v arg1="$i" -N M_${i:0:6} /lustre2/scratch/Vago/160/Haloplex/scripts_160/FreeBayes_v1.0.2.MBC.Multi.Naive.sh;done

##################################################################################################################

DP=/home/gbucci/Dropbox-Uploader/dropbox_uploader.sh

$DP -p -h upload x??.vcf.gz*  /WORKSPACE/ciceri/160_RelapsingLeukemia/Haloplex_HS3
#cambia path, non riconosce lo spazio

##################################################################################################################
#REGION FILTERING
mkdir  /lustre2/scratch/Vago/160/Haloplex/HS3
#cd  /lustre2/scratch/Vago/160/Haloplex/HS3
cd /lustre2/scratch/Vago/160/Haloplex/HS3/All_70
#ln /lustre2/raw_data/170717_SN859_0458_AHM55WBCXY/Ciceri_160_Realpsing_Leukemia/Renamed/BBDUK/BAM/CLIPPED_NM7/MARKED/CHUNKS/*x??.vcf.gz*
mv *vcf.gz* /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/170717_SN859_0458_AHM55WBCXY/Ciceri_160_Realpsing_Leukemia/Renamed/BBDUK/BAM/CLIPPED_NM7/MARKED/VCF/
ln -s /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/170717_SN859_0458_AHM55WBCXY/Ciceri_160_Realpsing_Leukemia/Renamed/BBDUK/BAM/CLIPPED_NM7/MARKED/VCF/*x??.vcf.gz* .

#- BED Covered

FILE=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Covered.bed
for i in `ls x??.vcf.gz`
do
     qsub -v arg1="$i",arg2="$FILE" -N ${i%%.vcf.gz} /lustre2/scratch/Vago/160/Haloplex/scripts_160/SNPSIFTINTERVAL.sh
done

# - LCR regions

for i in `ls x??.IN.vcf.gz`
do
     qsub -v arg1="$i" -N ${i%%.vcf.gz} /lustre2/scratch/Vago/160/Haloplex/scripts_160/SNPFILTERLCR.sh
done
#for i in `ls x??.vcf.gz`; do zcat $i| snpSift filter "(CHROM='chr5')&(POS>170837530)&(POS<170837550)" > ${i%%.vcf.gz}.NPM1.vcf ;done
#- Merge Multi

vcf-concat x??.IN.LCR.vcf.gz | vcfsort| vcfuniq| vcffixup - > Multi.IN.LCR.vcf &
bgzip -f Multi.IN.LCR.vcf
tabix -f Multi.IN.LCR.vcf.gz

zgrep -c '^chr' Multi.IN.LCR.vcf.gz
#348653
#368230
#141944
#HS3 309920

zcat Multi.IN.LCR.vcf.gz| snpSift filter "(CHROM='chr5')&(POS>170837530)&(POS<170837550)"|\
vcfbreakmulti | bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa > Multi.IN.LCR.NPM1.vcf 

zcat Multi.IN.LCR.vcf.gz| vcfbreakmulti|bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa |\
bedtools intersect -header -b /lustre2/scratch/Vago/160/Haloplex/scripts_160/Mutazioni_note.bed -a - \
|bgzip -fc > Multi.IN.LCR.Mutazioni_Note.vcf.gz &

vcfannotate -k GENE_SYMBOL -b \
/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Covered.bed Multi.IN.LCR.Mutazioni_Note.vcf.gz \
| vcffilter -g "( AO > 0 )" | vcf2tsv -g > Mutazioni_note.xls

#vcfbreakmulti Multi.IN.LCR.vcf.gz | vcffilter -g "( AO > 0 )"|\
#bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa | vcfsort| vcfuniq|vcffixup - |bgzip -c > Multi.IN.LCR.NORM.vcf.gz

#zcat Multi.IN.LCR.NORM.vcf.gz| snpSift filter "(CHROM='chr5')&(POS>170837530)&(POS<170837550)"|\
#snpSift extractFields - "CHROM" "POS" "REF" "ALT" "GEN[*].GT"

#############################################################################################################################
#- HARD filter
# f5f6b6e..4911c52
qsub -v arg1="Multi.IN.LCR.vcf.gz" -N "DP" /lustre2/scratch/Vago/160/Haloplex/scripts_160/SNPSIFT_HARDFILTER_panels_multi.sh 

# vcfbreakmulti $targ | vcffilter -f " ( AO > 0 ) " | \
# vcffilter -f "( QUAL > 0 ) & ( SAF > 0 & SAR > 0  ) & ( RPR > 1 | RPL > 1 ) & ( MQM > 10 & MQMR > 10 )" | \
# java -jar /usr/local/cluster/src/snpEff/SnpSift.jar filter \
# "(( GEN[ANY].DP[ANY] >= 4 ) & (GEN[ANY].AO[ANY] >= 1) & ( GEN[ANY].QA[ANY] > 0 ))" | \
# bgzip -c > ${targ%%.vcf.gz}.unBias.vcf.gz

zgrep -c '^chr' Multi.IN.LCR.unBias.vcf.gz

#############################################################################################################################
#Annotation (not Effect)
#86509bd..069aa23
qsub -v arg1="Multi.IN.LCR.unBias.vcf.gz" -N "Ann" /lustre2/scratch/Vago/160/Haloplex/scripts_160/SNPSIFT_multi.sh

zgrep -c "^chr" Multi.IN.LCR.unBias.Anno.vcf.gz

#132824
#############################################################################################################################

mkdir BYSAMPLE
cd BYSAMPLE/
ln -s ../Multi.IN.LCR.unBias.Anno.vcf.gz* .
vcfsamplenames Multi.IN.LCR.unBias.Anno.vcf.gz| sort > Names.txt

bedtools intersect -header -a Multi.IN.LCR.unBias.Anno.vcf.gz \
-b /lustre2/scratch/Vago/160/Haloplex/scripts_160/Mutazioni_note.bed  \
> Mutazioni_note.vcf
bgzip Mutazioni_note.vcf
tabix Mutazioni_note.vcf.gz

zgrep -c "^chr" Mutazioni_note.vcf.gz 

vcfannotate -k GENE_SYMBOL -b \
/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Covered.bed Mutazioni_note.vcf.gz \
| vcffilter -g "( AO > 0 )" | vcf2tsv -g > Mutazioni_note.xls
#24

vcfannotate -k GENE_SYMBOL -b \
/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Covered.bed Multi.IN.LCR.unBias.Anno.vcf.gz \
| vcffilter -g "( AO > 0 )" | vcf2tsv -g > Mutazioni_note.xls
#############################################################################################################################
# 069aa23..8b499a4 
i=Multi.IN.LCR.unBias.Anno.vcf.gz
qsub -v arg1="$i" -N "Eff" /lustre2/scratch/Vago/160/Haloplex/scripts_160/SNPSNPEFF_multi.sh

zgrep -c "^chr" Multi.IN.LCR.unBias.Anno.Eff.vcf.gz
#132824
#############################################################################################################################
#POPULATION FREQUENCIES

vcfkeepinfo Multi.IN.LCR.unBias.Anno.Eff.vcf.gz "AC" "AF" "NS" "AN"|vcffilter -f " ( AF > 0 ) "  | vcfbreakmulti|\
bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa |vcfsort|vcfuniq|vcffixup -| cut -f 1-8 > HS3_pop.vcf

vcffilter -f " ( AF > 0.90 ) " HS3_pop.vcf | vcf2bed > HS3_most_frequent.bed

#############################################################################################################################
#SPLIT BY PATIENTS
#filter out variant with less than 1 supporting barcode per altenate allele (remove variants which were called by other samples and not present in this one) GEN.AO >= 1

cat Names.txt | while read name; do qsub -v arg1=Multi.IN.LCR.unBias.Anno.Eff.vcf.gz,arg2="$name" -N ${name:0:6} \
/lustre2/scratch/Vago/160/Haloplex/scripts_160/Split.sh ; done

cat Names.txt| while read i
do
	printf "%s\t" $i ; zgrep -c "^chr" ${i}.Multi.IN.LCR.unBias.Anno.Eff.vcf.gz
done

ADD PRE READ COUNTS
Freebayes PILEUP Call MultiSample Haloplex HS1 Revison 11/01/17

#############################################################################################################################
#PILEUP
#the call is identical except the use of -4 (use duplicates) option

mkdir /lustre2/scratch/Vago/160/Haloplex/HS3/PILEUP
mkdir /lustre2/scratch/Vago/160/Haloplex/HS3/All_70/PILEUP/
cp /lustre2/raw_data/170717_SN859_0458_AHM55WBCXY/Ciceri_160_Realpsing_Leukemia/Renamed/BBDUK/BAM/CLIPPED_NM7/MARKED/BAMLIST.txt .
ln -s /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/170717_SN859_0458_AHM55WBCXY/Ciceri_160_Realpsing_Leukemia/Renamed/BBDUK/BAM/CLIPPED_NM7/MARKED/*bam* .
ln -s /lustre2/scratch/Vago/160/Haloplex/161102_SN859_0372_AHWF5JBCXX/FASTQ/BBDUK/BAM/CLIPPED_NM7/MARKED/CHUNKS/x?? .

for i in `ls x??`;do qsub -v arg1="$i" -N M_${i:0:6} \
/lustre2/scratch/Vago/160/Haloplex/scripts_160/FreeBayes_v1.0.2.MBC.Multi.PILEUP.sh;done

#TARGET
FILE=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Covered.bed
for i in `ls x??.pre.vcf.gz`
do
     qsub -v arg1="$i",arg2="$FILE" -N ${i%%.vcf.gz} /lustre2/scratch/Vago/160/Haloplex/scripts_160/SNPSIFTINTERVAL.sh
done

#LCR
for i in `ls x??.pre.IN.vcf.gz`
do
     qsub -v arg1="$i" -N ${i%%.vcf.gz} /lustre2/scratch/Vago/160/Haloplex/scripts_160/SNPFILTERLCR.sh
done

condeactivate #need Vcf.pm in @INC
vcf-concat x??.pre.IN.vcf.gz | vcfsort | vcfuniq | vcffixup - > Multi.pre.IN.LCR.vcf

bgzip -f Multi.pre.IN.LCR.vcf
tabix -f Multi.pre.IN.LCR.vcf.gz

zgrep -c '^chr' Multi.pre.IN.LCR.vcf.gz
#258345
#260477

condactivate

vcfsamplenames Multi.pre.IN.LCR.vcf.gz > Names.txt

cat Names.txt | while read name; do qsub -v arg1=Multi.pre.IN.LCR.vcf.gz,arg2="$name" \
-N ${name:0:6} /lustre2/scratch/Vago/160/Haloplex/scripts_160/Split.sh ; done

for i in `ls *Multi.pre.IN.LCR.vcf.gz`; do tabix -f $i; done

#VAF x sciClone
# cat Names.txt| while read i 
# do	
# 	zcat ${i}.Multi.pre.IN.LCR.vcf.gz| snpSift filter "isVariant(GEN[0])&(GEN[0].QA>30)&(GEN[0].AO>6)&(GEN[0].DP>100)"| \
# 	snpSift extractFields -e "." - \
# 	"CHROM" "POS" "GEN[0].RO" "GEN[0].AO"|  grep -v "#" |\
# 	awk '{OFS="\t"; print $1,$2,$3,$4,$3/($4+0.0001)}'  > ${i%%.Multi.pre.IN.LCR.vcf.gz}.vaf
# done

#create a bed file with name corresponding to pre DP_RO_AO
cat Names.txt | while read i;
do vcfkeepgeno ${i}.Multi.pre.IN.LCR.vcf.gz "DP" "RO" "AO" |vcfkeepinfo - ""| vcf2bed | cut -f 1,2,3,11  > Pre.${i}.bed
done
#############################################################################################################################
#ADD PRE COUNTS DP_RO_AO TO MBC CALL
#/lustre2/scratch/Vago/160/Haloplex/HS3/All_70/PILEUP
cd /lustre2/scratch/Vago/160/Haloplex/HS3/All_70/BYSAMPLE

cat Names.txt | while read i;
	do vcfannotate -k PRE -b /lustre2/scratch/Vago/160/Haloplex/HS3/All_70/PILEUP/Pre.${i}.bed ${i}.Multi.IN.LCR.unBias.Anno.Eff.vcf.gz > ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.vcf
done


cat Names.txt | while read i;
do
	bgzip -f ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.vcf
	tabix -f ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.vcf.gz
done 

#files in /lustre2/scratch/Vago/160/Haloplex/HS3/All_70/BYSAMPLEMulti.IN.LCR.unBias.Anno.Eff.En.Pre.vcf.gz
#are my final target
	
#############################################################################################################################

6) TIER1
#pwd /lustre2/scratch/Vago/160/Haloplex/HS3/All_70/BYSAMPLE

for i in `ls *.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.vcf.gz`;
do zcat $i |snpSift varType - | vcffilter -g " ( ( AO / ( RO + AO ) ) > 0.02 ) & ( AO > 0 ) "| \
bgzip -c > ${i%%.vcf.gz}.tier1.vcf.gz ;
tabix -f ${i%%.vcf.gz}.tier1.vcf.gz
done &

#snpSift annotate /lustre1/genomes/hg19/annotation/COSMIC/b37_cosmic_v54_120711.vcf.gz

#cat VCF/Stat_counts.Tier1.txt| cut -dz -f 2|awk '{ total += $1; count++ } END { print total/count }'

7) TIER2
#remove healthy control shared variants, 1000genomes annotated and synonymous except COSMIC
#here we could use healthy from HS2
HC135=/lustre2/scratch/Vago/160/Haloplex/161102_SN859_0372_AHWF5JBCXX/FASTQ/BBDUK/BAM/CLIPPED_NM7/MARKED/CHUNKS/BYSAMPLE/HC224_S53_R1.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier1.vcf.gz 
HC224=/lustre2/scratch/Vago/160/Haloplex/HS3/All_70/BYSAMPLE/HC224_S53_R1.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier1.vcf.gz

#change HS1_most_frequent.bed in HS3_most_frequent.bed
#filter on non-silent and 1000genome

for i in `ls *.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier1.vcf.gz`;
do vcfintersect -b HS1_most_frequent.bed -v ${i} | echo ${i%%.tier1.vcf.gz}; zcat $i |\
bedtools intersect -header  -v -a - -b $HC224 |\
snpSift filter -n "((exists KGPhase1) | (exists KGPhase3))" | \
snpSift filter -n "(EFF[0] =~ 'synonymous')" | bgzip -f > ${i%%.tier1.vcf.gz}.tier2.vcf.gz; tabix -f ${i%%.tier1.vcf.gz}.tier2.vcf.gz ; done &
#keep safe COSMIC
for i in `ls *Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier1.vcf.gz`; do echo ${i%%.tier1.vcf.gz}; zcat $i |snpSift filter "ID =~ 'COSM'"| bgzip -fc > ${i%%.tier1.vcf.gz}.COSMIC.vcf.gz; tabix -f ${i%%.tier1.vcf.gz}.COSMIC.vcf.gz ; done &

condeactivate 
for i in `ls *Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier2.vcf.gz`; 
do 
	echo ${i%%.tier2.vcf.gz}; 
	vcf-concat $i ${i%%.tier2.vcf.gz}.COSMIC.vcf.gz|\
	vcfsort| bgzip -fc > ${i%%.tier2.vcf.gz}.tier2.1.vcf.gz; 
	tabix -f ${i%%.tier2.vcf.gz}.tier2.1.vcf.gz ; 
done 

8) Tier3

for i in `ls *Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier2.1.vcf.gz`; 
do 
	echo ${i%%.tier2.1.vcf.gz};zcat $i |\
	vcffixup - | vcffilter -g "( ( AO / ( RO + AO ) ) > 0.02 ) & ( AO > 0 )" |\
	vcffixup -| snpSift filter "(EFF[ANY] has 'MODERATE')||(EFF[ANY] has 'HIGH')||(EFF[ANY] =~ 'splice')" |\
	vcfsort| vcfuniq|bgzip -fc > ${i%%.tier2.1.vcf.gz}.tier3.vcf.gz && tabix -f ${i%%.tier2.1.vcf.gz}.tier3.vcf.gz; 
done 

#############################################################################################################################
#Make a targeted call on tier3 and retrieve pre MBC AO

cd ..
#pwd
#/lustre2/scratch/Vago/160/Haloplex/HS3

mkdir RECALL
cd RECALL
cp ../BYSAMPLE/Names.txt .
cat Names.txt| wc 
#30
#70
ln -s /lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/170717_SN859_0458_AHM55WBCXY/Ciceri_160_Realpsing_Leukemia/Renamed/BBDUK/BAM/CLIPPED_NM7/MARKED/*bam* .

#create target VCF for the targeted recall; use tier3 
#PRE means with PRE counts
condactivate
cat Names.txt | while read i;
do 
	vcfkeepgeno ../BYSAMPLE/${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.vcf.gz "" |\
	vcfkeepinfo - ""|vcfbreakmulti|\
	bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa |vcfsort|vcfuniq|vcffixup -|cut -f 1-7  > ${i}.vcf
done

ls *vcf|wc
#30
#70

for i in `ls *vcf`;do echo $i;grep -c -v "#" $i; done 
for i in `ls ../BYSAMPLE/*Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.vcf.gz`;do echo $i;zgrep -c -v "#" $i; done

#Do second round of freebayes on selected tier3 regions
ls *bam|wc
#70

cat Names.txt | while read i;
do qsub -v arg1="${i}.vcf",arg2="${i%%_*}_sorted.bam" -N M_${i:0:6} /lustre2/scratch/Vago/160/Haloplex/scripts_160/FreeBayes_v1.0.2.MBC.Pre.RECALL.sh;done
#the call is identical except the use of -4 (use duplicates) option

#procedo con quelli gia` pronti
#keep AF
cat Names.txt | while read i;
do 
	echo $i
	vcfkeepgeno ${i}.vcf.Pre.vcf.gz "DP" "RO" "AO" |\
	vcfkeepinfo - ""|vcfbreakmulti|\
	bcftools norm -cs -f /lustre1/genomes/hg19/fa/hg19.fa |\
	vcfsort|vcfuniq|vcf2bed | cut -f 1,2,3,11 | sed  's/\:/_/g'   > ${i}.bed
done

#copy here the post MBC tier3 calls and annotate with RECALL counts (pre MBC)
#pwd /lustre2/scratch/Vago/160/Haloplex/HS3/All_70/RECALL

cp ../BYSAMPLE/*.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.vcf.gz* .
 
cat Names.txt | while read i;
do vcfannotate -k PREMBC -b ${i}.bed ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.vcf.gz | snpSift filter "GEN[0].AO >= 1 " > ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.vcf
bgzip -f ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.vcf
tabix -f ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.vcf.gz
done

export PERL5LIB=/usr/local/cluster/bin/
vcf-merge *Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.vcf.gz |\
vcfsort | vcfuniq| vcffixup - | bgzip -fc >  Combined_Tier3.vcf.gz && tabix -f Combined_Tier3.vcf.gz



#COUNT HLALOSS specific variants
#COUNT HLALOSS specific variants 
#IN ATTTESA DI ELENCO PAZIENTI (12/09/17)

#############################################################################################################################
#ALTERNATIVA per evitare di ricontare i large indel

#chr7_151945070_151945071 GTTA 86
#45190

mkdir New_Combined_Tier3
cd New_Combined_Tier3

#estraggo le regioni in cui c'e' almeno una variante in tier3
snpSift filter " GEN[*].AO[*] > 1 " ../Combined_Tier3.vcf.gz | vcfbreakmulti|bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa |vcfsort|vcfuniq|vcffixup - | vcf2bed --do-not-sort > Combined_Tier3.bed
#le intersecco col file pre tiering con tutti i campioni
#vcfintersect -b Combined_Tier3.bed ../Multi.IN.LCR.unBias.Anno.Eff.vcf.gz |bgzip -fc > Combined_Tier3.vcf.gz ;tabix -f Combined_Tier3.vcf.gz
vcfintersect -b Combined_Tier3.bed ../../Multi.IN.LCR.unBias.Anno.vcf.gz |bgzip -fc > Combined_Tier3.vcf.gz ;tabix -f Combined_Tier3.vcf.gz

zcat Combined_Tier3.vcf.gz| vcffilter -g " AO > 0 " | snpSift extractFields - "CHROM" "POS" "ALT" "GEN[*].AO[*]"| \
grep -v "#"|awk '{OFS="\t";print $1,$2-1,$2,$1"_"$2-1"_"$2"_"$3,NF-3}'|grep -v "CHROM" | sort -u -k 1,1 -k 2,2n -k 4,4  > Combined_Tier3.bed
#snpSift filter " GEN[*].AO[*] > 1 " Combined_Tier3.vcf.gz | snpSift extractFields - "CHROM" "POS" "ALT" "GEN[*].AO[*]"| grep -v "#"|awk '{OFS="\t";print $1,$2-1,$2,$1"_"$2-1"_"$2"_"$3,NF-3}' > Combined_Tier3_AC.bed

#zcat ../Combined_Tier3.vcf.gz | vcf2bed --do-not-sort  | awk '{OFS="\t"; print $1,$2,$3,$1"_"$2"_"$3"_"$7}'> Combined_Tier3_AC.bed 
#paste -d "\t" Combined_Tier3_AC.txt Combined_Tier3_AC.bed | awk '{OFS="\t"; print $2,$3,$4,$5,$1}' | sort -u -k 1,1 -k 2,2n -k 4,4  > Combined_Tier3.bed

bgzip -f Combined_Tier3.bed;tabix -f Combined_Tier3.bed.gz
cp ../Names.txt .

#Alternate counts on HS3 tier3
cat Names.txt | while read i;
do zcat ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.vcf.gz | vcf2bed --do-not-sort| awk '{OFS="_"; print $1,$2,$3,$7}' > ${i}_AC.txt
done


# cat Names.txt | while read i;
# do
# 	zgrep -w -f ${i}_AC.txt Combined_Tier3.bed.gz| awk '{OFS="\t"; print $1,$2,$3,$4"_"$5}' | bgzip -cf > ${i}.bed.gz; tabix -f  ${i}.bed.gz
# done

cat Names.txt | while read i; do
    if [ -s ${i}.bed.gz ]
	then echo ${i}.bed.gz
	else 
	   /lustre2/scratch/Vago/160/Haloplex/scripts_160/Zgrep_AC.sh ${i}
    fi
done
#this execute zgrep -w -f ${i}_AC.txt Combined_Tier3.bed.gz > ${i}.txt

cat Names.txt | while read i;
do
	cat ${i}.txt | awk '{OFS="\t"; print $1,$2,$3,$4"_"$5}' | bgzip -cf > ${i}.bed.gz; tabix -f  ${i}.bed.gz
done

zgrep -c "^chr" *R1.bed.gz | sed -s 's/:/\t/' > HS3_Variants_counts.txt

cat Names.txt | while read i;
do
	KASP=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/KASP.bed
	vcfannotate -k HS3_AC -b <(zcat ${i}.bed.gz)  ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.vcf.gz| vcfannotate -k KASP -b $KASP| snpSift filter "GEN[0].AO >= 1 " > ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3AC.vcf
	bgzip -f ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3AC.vcf
	tabix -f ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3AC.vcf.gz
done

##HLALOSS
#per il HS3 ho solo tre campioni
grep -f hlaloss.txt Names.txt > HLALOSS.txt

vcfkeepsamples ../Combined_Tier3.vcf.gz `cat HLALOSS.txt` > ../HLALOSS.vcf
bgzip ../HLALOSS.vcf; tabix ../HLALOSS.vcf.gz
#snpSift filter " GEN[*].AO[*] > 1 "  ../HLALOSS.vcf.gz |snpSift extractFields - "CHROM" "POS" "ALT" "GEN[*].AO[*]"| grep -v "#"|awk '{OFS="\t";print $1,$2-1,$2,$1"_"$2-1"_"$2"_"$3,NF}' > HLALOSS_AC.bed
zcat ../HLALOSS.vcf.gz |vcffilter -g " AO > 2 " | snpSift extractFields - "CHROM" "POS" "ALT" "GEN[*].AO[*]"| grep -v "#"|awk '{OFS="\t";print $1,$2-1,$2,$1"_"$2-1"_"$2"_"$3,NF-3}' > HLALOSS_AC.bed
#zcat ../HLALOSS.vcf.gz | vcf2bed --do-not-sort  | awk '{OFS="\t"; print $1,$2,$3,$1"_"$2"_"$3"_"$7}'> HLALOSS_AC.bed 
#paste -d "\t" HLALOSS_AC.txt HLALOSS_AC.bed | awk '{OFS="\t"; print $2,$3,$4,$5,$1}' | sort -u -k 1,1 -k 2,2n -k 4,4  > HLALOSS.bed

#tolgo header e ordino
sort -u -k 1,1 -k 2,2n -k 4,4 HLALOSS_AC.bed|grep -v CHROM > HLALOSS.bed
bgzip -f HLALOSS.bed;tabix -f HLALOSS.bed.gz

#troppo lento uso bedtools
# cat Names.txt | while read i;
# do 
# 	zgrep -w -f ${i}_AC.txt HLALOSS.bed.gz| awk '{OFS="\t"; print $1,$2,$3,$4"_"$5}' | bgzip -cf > ${i}.HL.bed.gz; tabix -f  ${i}.HL.bed.gz
# 	vcfannotate -k HLALOSS_AC -b <(zcat ${i}.HL.bed.gz) ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS2AC.vcf.gz | snpSift filter "GEN[0].AO >= 1 " > ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf
# 	bgzip -f ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf
# 	tabix -f ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz
# done

#trova le intersezioni tra le varianti di ogni singolo e quelle trovate negli hlaloss sulla base della coordinata. 
#puo' creare problemi con indels
#devo trovare un metodo diverso partendo da VCF

cat Names.txt | while read i;
do
	bedtools intersect -wa -b ${i}.bed.gz -a  HLALOSS.bed.gz|bedtools sort |awk '{OFS="\t"; print $1,$2,$3,$4"_"$5}'|bgzip -cf > ${i}.HL.bed.gz; tabix -f  ${i}.HL.bed.gz
	vcfannotate -k HLALOSS_AC -b <(zcat ${i}.HL.bed.gz) ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3AC.vcf.gz | snpSift filter "GEN[0].AO >= 1 " > ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf
	bgzip -f ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf
	tabix -f ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz
done


#############################################################################################################################
#Calculate VAF
cd ../
#/lustre2/scratch/Vago/160/Haloplex/HS3/All_70/RECALL

#BLOCK
cat Names.txt | while read i;
do 
	snpSift extractFields ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz \
	-e '.' "CHROM" "POS" "ID" "QUAL" "AO" "RV" "REF" "ALT" "VARTYPE" \
	"KGPhase1" "KGPhase3" "COMMON" "HLALOSS_AC" "PREMBC" "dbNSFP_ExAC_AF[0]" \
	"dbNSFP_ExAC_AC[0]" "dbNSFP_CADD_phred" "dbNSFP_ESP6500_AA_AF" "dbNSFP_FATHMM_pred" \
	"dbNSFP_MutationTaster_pred" "G5A" "PM" "PMC" "OM" "GENE" "ABP" "CIGAR" "GEN[0].AO[0]" \
	"GEN[0].RO[0]" "GEN[0].DP[0]" "GQ" "GL" "QA" "ANN[0].GENE" "EFF[0]"| grep -v "#" \
	|awk '{OFS="\t"; print $28/($30+0.0001)}' > ${i}.vaf
	printf "%s\n" "#VAF" `cat ${i}.vaf` > ${i}.vaf
	snpSift extractFields ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz \
	-e '.' "CHROM" "POS" "ID" "QUAL" "AO" "RV" "REF" "ALT" "VARTYPE" "KGPhase1" "KGPhase3" \
	"COMMON" "PREMBC" "dbNSFP_ExAC_AF[0]" "dbNSFP_ExAC_AC[0]" "dbNSFP_CADD_phred" \
	"dbNSFP_ESP6500_AA_AF" "dbNSFP_FATHMM_pred" "dbNSFP_MutationTaster_pred" "G5A" \
	"PM" "PMC" "OM" "GENE" "ABP" "CIGAR" "GEN[0].AO[0]" "GEN[0].RO[0]" "GEN[0].QA[0]" \
	"GEN[0].DP[0]" "GEN[0].GQ[0]" "GEN[0].GL[0]" "KASP" "ANN[0].GENE" "EFF[0]" > ${i}.tsv
	snpSift extractFields ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz -e '0' "HS3_AC" | cut -d_ -f 5 | grep -v '^$' | cut -d: -f 1| sed '1s/^/#HS3_AC\n/' > ${i}.ac
	snpSift extractFields ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz -e '0' "HLALOSS_AC" | cut -d_ -f 5  | grep -v '^$' | cut -d: -f 1| sed '1s/^/#HLALOSS_AC\n/' > ${i}.HL.ac
	paste -d "\t" ${i}.ac ${i}.HL.ac ${i}.vaf ${i}.tsv > ${i}.tier3.tsv
done 
#BLOCK

cat Names.txt | while read i;
do
	KASP=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/KASP.bed
	zcat ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz| vcfannotate -k KASP -b $KASP > ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf
	bgzip -f ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf
	tabix -f ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz 
done

####### ###### ######  15 09 2017

##COMBINED
#export PERL5LIB=/usr/local/cluster/bin/

mv HC224_S53_R1.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz HC224_S53_R1.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.bak
vcf-merge *Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz|\
vcfbreakmulti|bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa |vcfsort|vcfuniq|vcffixup - |\
bgzip -fc >  Combined_Tier3.HS3ACHL.vcf.gz && tabix -f Combined_Tier3.HS3ACHL.vcf.gz &  

tabix Combined_Tier3.HS3ACHL.vcf.gz

KASP=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/KASP.bed
zcat Combined_Tier3.HS3ACHL.vcf.gz| vcfannotate -k KASP -b $KASP > Combined_Tier3.HS3ACHL.vcf
bgzip -f Combined_Tier3.HS3ACHL.vcf
tabix -f Combined_Tier3.HS3ACHL.vcf.gz 


bedtools intersect -header -b /lustre2/scratch/Vago/160/Haloplex/scripts_160/Mutazioni_note.bed\
 -a Combined_Tier3.HS3ACHL.vcf.gz | bgzip -fc > Mutazioni_note.vcf.gz && tabix -f Mutazioni_note.vcf.gz

vcfbreakmulti Mutazioni_note.vcf.gz | vcfsort| vcffixup - |\
snpSift extractFields  -e '.' -  "CHROM" "POS" "ID" "QUAL" "AO" "RV" "REF" "ALT"  "VARTYPE"  "KGPhase1" "KGPhase3" "COMMON"   "PREMBC" "dbNSFP_ExAC_AF[0]" "dbNSFP_ExAC_AC[0]" "dbNSFP_CADD_phred"  "dbNSFP_ESP6500_AA_AF" "dbNSFP_FATHMM_pred"  "dbNSFP_MutationTaster_pred"  "G5A" "PM" "PMC" "OM" "GENE" "GEN[*].AO[0]" "GEN[*].RO[0]" "GEN[*].DP[0]" "ABP" "CIGAR" "GQ" "GL" "QA" "KASP"  "ANN[0].GENE" "EFF[0]" >  Mutazioni_note.tsv

#############################################################################################################################
#check 
cat Names.txt | while read i;
do echo ${i}
snpSift extractFields ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz "ALT" "ANN[0].ALLELE" | awk '{if ($1==$2) print "EQUAL";else print "DIFFERENT"}' | grep DIFFERENT -c
done

#VAF
cat Names.txt | while read i;
do snpSift extractFields ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz \
-e '.' "CHROM" "POS" "ID" "QUAL" "AO" "RV" "REF" "ALT" "VARTYPE" \
"KGPhase1" "KGPhase3" "COMMON" "HLALOSS_AC" "PREMBC" "dbNSFP_ExAC_AF[0]" \
"dbNSFP_ExAC_AC[0]" "dbNSFP_CADD_phred" "dbNSFP_ESP6500_AA_AF" "dbNSFP_FATHMM_pred" \
"dbNSFP_MutationTaster_pred" "G5A" "PM" "PMC" "OM" "GENE" "ABP" "CIGAR" "GEN[0].AO[0]" \
"GEN[0].RO[0]" "GEN[0].DP[0]" "GQ" "GL" "QA" "ANN[0].GENE" "EFF[0]"| grep -v "GEN"| \
awk '{OFS="\t"; print $28/($30+0.0001)}'  > ${i}.vaf
printf "%s\n" "#VAF" `cat ${i}.vaf` > ${i}.vaf
done 

#Merge VAF and AC
cat Names.txt | while read i;
do snpSift extractFields ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz \
-e '.' "CHROM" "POS" "ID" "QUAL" "AO" "RV" "REF" "ALT" "VARTYPE" "KGPhase1" "KGPhase3" \
"COMMON" "PREMBC" "dbNSFP_ExAC_AF[0]" "dbNSFP_ExAC_AC[0]" "dbNSFP_CADD_phred" \
"dbNSFP_ESP6500_AA_AF" "dbNSFP_FATHMM_pred" "dbNSFP_MutationTaster_pred" "G5A" \
"PM" "PMC" "OM" "GENE" "ABP" "CIGAR" "GEN[0].DP[0]" "GEN[0].RO[0]" "GEN[0].AO[0]" "GEN[0].QA[0]" \
"GEN[0].DP[0]" "GEN[0].GQ[0]" "GEN[0].GL[0]" "KASP" "ANN[0].GENE" "EFF[0]" > ${i}.tsv
snpSift extractFields ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz -e '0' "HS3_AC" | cut -d_ -f 5 | grep -v '^$' | cut -d: -f 1| sed '1s/^/#HS3_AC\n/' > ${i}.ac
 snpSift extractFields ${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.HS3ACHL.vcf.gz -e '0' "HLALOSS_AC" | cut -d_ -f 5  | grep -v '^$' | cut -d: -f 1| sed '1s/^/#HLALOSS_AC\n/'> ${i}.HL.ac
paste -d "\t" ${i}.ac ${i}.HL.ac ${i}.vaf ${i}.tsv > ${i}.tier3.tsv
done &

for i in `ls *tsv`; do sed -i 's/HS3_AC/HS3_AC/' $i; done


####PULIZIA

cd /lustre2/scratch/gbucci/Vago/160/Haloplex/May_2018
ln -s ../HS3/All_70/RECALL/*HS3ACHL.vcf.gz* .
ln -s ../160630_SN859_0309_AHJ2KVBCXX/Agilent_MBC_Mark/FASTQ/BBDUK/BAM/CLIPPED_NM7/MARKED/CHUNKS/BYSAMPLE/*HS1ACHL.vcf.gz* .
ln -s ../161102_SN859_0372_AHWF5JBCXX/FASTQ/BBDUK/BAM/CLIPPED_NM7/MARKED/CHUNKS/BYSAMPLE/*HS2ACHL.vcf.gz* .
grep -f HS2_List.txt HS3_list.txt |while read i; do ls ${i}*HS2ACHL.vcf.gz; unlink ${i}*HS2ACHL.vcf.gz;done
grep -f HS1_List.txt HS3_list.txt |while read i; do ls ${i}*HS1ACHL.vcf.gz; unlink ${i}*HS1ACHL.vcf.gz;done

for i in `ls *CHL.vcf.gz`; do a=`echo $i|cut -d_ -f1|cut -d. -f 1`; echo $a; ln -s $i ${a}.vcf.gz; done
for i in `ls *CHL.vcf.gz.tbi`; do a=`echo $i|cut -d_ -f1|cut -d. -f 1`; echo $a; ln -sf $i ${a}.vcf.gz.tbi; done

cat Names.txt| while read a
do 
	echo $a
	i=${a}.vcf.gz
	echo $i
	zcat $i |snpSift filter "( (GEN[*].AO[0] > 2) & (GEN[*].DP > 4) & (GEN[*].DP < 100) ) || ( (GEN[*].AO[0] > 3) & (GEN[*].DP < 5000) & (GEN[*].DP > 100) )" | bgzip -fc > ${a}.GTFIX.vcf.gz
	tabix -f ${a}.GTFIX.vcf.gz
done


for i in `ls *GTFIX.vcf.gz`
do
	zcat ${i}| vcfbreakmulti|bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa | \
	vcfsort | vcfuniq| vcffixup - | vcfglxgt|bgzip -fc > ${i%%GTFIX.vcf.gz}norm.vcf.gz
	tabix -f ${i%%GTFIX.vcf.gz}norm.vcf.gz
done


#MAF and VEP
#PREPARE SAMPLES FOR MAF

# for i in `ls *{1,2,4}.recall.norm.vep.vcf.gz`
# do
# 	zcat $i | sed 's/^chr//' > ${i%%.recall.norm.vep.vcf.gz}.maf.vcf
# done
#ANNOTATE BY SAMPLE

/home/gbucci/vep/variant_effect_predictor.pl -i FOCA3.norm.vcf.gz --assembly GRCh37 --cache_version 90 --cache --offline  --fasta /home/gbucci/.vep/homo_sapiens/90_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa

# for i in `ls *norm.vcf.gz`
#  do
#  	/home/gbucci/.conda/envs/gatk-3.6/bin/perl /home/gbucci/vep/variant_effect_predictor.pl \
#  	-i ${i} --vcf -o  ${i%%recall.norm.vcf.gz}recall.norm.vep.vcf \
#  	--everything --tab -o ${i%%recall.norm.vcf.gz}recall.norm.vep \
#  	--assembly GRCh37 --cache_version 90 --cache --offline  --fasta /home/gbucci/.vep/homo_sapiens/90_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.f
#  	bgzip ${i%%recall.norm.vcf.gz}norm.vep.vcf
#  	tabix ${i%%recall.norm.vcf.gz}norm.vep.vcf.gz
#  done 
 
cat Names.txt| while read i 
do
	zcat ${i}.norm.vep.vcf.gz | sed 's/^chr//' > ${i%%.norm.vep.vcf.gz}.maf.vcf
done

#in realta` se no gzippatei si puo' usare il vcf normale senza passare da VEP
#ci pensa lo script

#for i in `ls *maf.vcf`
for i in `ls *vep.vcf`
do
	/lustre2/scratch/gbucci/vcf2maf-master/vcf2maf.pl --input-vcf  ${i} \
 	--output-maf ${i%%.vep.vcf}.maf --maf-center CTGB --tumor-id ${i%%.vep.vcf} \
  	--ref-fasta /home/gbucci/.vep/homo_sapiens/90_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa \
  	--species homo_sapiens --ncbi-build GRCh37 --cache-version 90
done 


gunzip FEAN167.norm.vcf.gz

/lustre2/scratch/gbucci/vcf2maf-master/vcf2maf.pl --input-vcf  FEAN167.norm.vcf  \
--output-maf FEAN167.norm.maf --maf-center CTGB --tumor-id FEAN167 --vcf-tumor-id FEAN167 \
--ref-fasta /home/gbucci/.vep/homo_sapiens/90_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa \
--species homo_sapiens --ncbi-build GRCh37 --cache-version 90

bgzip FEAN167.norm.vcf 

for i in *{1,2}.maf; do cat $i |grep -v "#" |unhead; done  > All.txt
cat <(head -n 2 FOCA1.maf) All.txt > All.maf
 




	
############################################
#MLL
#MLLexons.bed
chr11   118342353       118345053       MLL_EXON3
chr11   118373091       118377381       MLL_EXON27

source=/lustre1/workspace/Ciceri/Ciceri_160_RelapsingLeukemia/170717_SN859_0458_AHM55WBCXY/Ciceri_160_Realpsing_Leukemia/Renamed/BBDUK/BAM/CLIPPED_NM7/MARKED/
find ${source} -maxdepth 1 -type f -name "*.bam"  |sort -u > BAMLIST.txt

cat BAMLIST.txt|while read i; do A=`basename $i`; echo ${A%%_sorted.bam}; done|sort -u > Names.txt

bedtools multicov -D -q 20 -bams `cat BAMLIST.txt` -bed MLLexons.bed  > MLLexons.D.cov &
bedtools multicov -q 20 -bams `cat BAMLIST.txt` -bed MLLexons.bed  > MLLexons.cov &
############################################
#FLT3-ITD
#FLT3-ITD.bed
chr13:28607161-28609590

cd /lustre2/raw_data/${RUN}/Ciceri_160_Realpsing_Leukemia/Renamed/BBDUK/BAM/CLIPPED_NM7

#for i in `cat Names.txt`; do samtools view -ub -L FLT3-ITD.bed ../${i}_sorted.bam > ${i}_FLT3.bam; samtools index ${i}_FLT3.bam; done &

for i in `ls *filtered_clipped.bam`; do a=${i%%_*}; printf "${i}\t300\t$a\n"; done | sort -u > config.txt

#only FLT3
pindel  -T 4 -f /lustre1/genomes/hg19/fa/hg19.fa -i config.txt -o FLT3 -L Pindel.log  -n 7 -M 2 -T 10 -x 3 -c chr13:28607161-28609589 &
	
bgzip tabix
targ=FLT3_SI.vcf.gz
snpEff -cancer -v -lof GRCh37.75 -dataDir /lustre1/ctgb-usr/local/src/snpEff/data/ -t -noLog -nodownload -v -lof GRCh37.75 $targ > FLT3_SI.Eff.vcf
zcat FLT3_SI.vcf.gz |snpEff  -v -lof GRCh37.75 -dataDir /lustre1/ctgb-usr/local/src/snpEff/data/ -t -noLog -nodownload -v -lof >FLT3_SI.Eff.vcf

vcfsamplenames FLT3_SI.Eff.vcf.gz > Names.txt
zcat  FLT3_SI.Eff.vcf.gz|snpSift varType -| snpSift extractFields -e '.' - "CHROM" "POS" "REF" "ALT" "ID" "VARTYPE" "ANN[0].GENE" "EFF[0]" "GEN[*].GT[*]" "GEN[*].AD[*]"  > FLT3_SI.Eff.tsv 
   	
########
#CONFRONTO
cd /lustre2/scratch/Vago/160/Haloplex/HS3/All_70/BYSAMPLE/Confronto
for i in `find /lustre2/scratch/Vago/160 -name "*Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.vcf.gz*"| grep -f Vecchi.txt` ; do ln -s $i .; done
for i in `find /lustre2/scratch/Vago/160 -name "*Multi.IN.LCR.unBias.tier3.vcf.gz*"| grep -f Vecchi.txt` ; do ln -s $i .; done
ls *tier3.vcf.gz|wc
#     40      40    2150

for i in `ls *Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.vcf.gz`
do
	zcat $i|snpSift filter "GEN[0].AO[0] > 2"|\
	vcfbreakmulti|bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa |vcfglxgt|vcfsort|vcfuniq|vcffixup -	| bgzip -c > ${i%%.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.vcf.gz}_HS2_GTFIX.vcf.gz
	tabix ${i%%.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.vcf.gz}_HS2_GTFIX.vcf.gz
done

for i in `ls *Multi.IN.LCR.unBias.tier3.vcf.gz`
do
	zcat $i|snpSift filter "GEN[0].AO[0] > 2"|\
	vcfbreakmulti|bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa |vcfglxgt|vcfsort|vcfuniq|vcffixup -	| bgzip -c > ${i%%.Multi.IN.LCR.unBias.tier3.vcf.gz}_HS2_GTFIX.vcf.gz
	tabix ${i%%.Multi.IN.LCR.unBias.tier3.vcf.gz}_HS3_GTFIX.vcf.gz
done
#
for i in `ls ../*Multi.IN.LCR.unBias.Anno.Eff.vcf.gz`
do
	zcat $i|snpSift filter "GEN[0].AO[0] > 2"|\
	vcfbreakmulti|bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa |vcfglxgt|vcfsort|vcfuniq|vcffixup -	| bgzip -c > ${i%%.Multi.IN.LCR.unBias.Anno.Eff.vcf.gz}_HS3_GTFIX.vcf.gz
	tabix ${i%%.Multi.IN.LCR.unBias.Anno.Eff.vcf.gz}_HS3_GTFIX.vcf.gz
done

cat Vecchi.txt | while read i; do  vcf-compare ${i}_HS2_GTFIX.vcf.gz ${i}*_HS3_GTFIX.vcf.gz|grep ^VN | cut -f 2-; done > Comparison.txt

vcfintersect -v NIMI105_HS2_GTFIX.vcf.gz -i NIMI105_S24_R1_HS3_GTFIX.vcf.gz -r /lustre1/genomes/hg19/fa/hg19.fa| vcffixup -| \
snpSift extractFields - -e "." "CHROM" "POS" "REF" "ALT" "GEN[0].QA" "EFF[0].GENE" "EFF[0].HGVS" "AA" "EFF[0].IMPACT" "GEN[0].GT" "GEN[0].AO"  > NIMI105-Hs2_private.tsv 

vcfintersect  NIMI105_HS2_GTFIX.vcf.gz -i NIMI105_S24_R1_HS3_GTFIX.vcf.gz -r /lustre1/genomes/hg19/fa/hg19.fa| vcffixup -| \
snpSift extractFields - -e "." "CHROM" "POS" "REF" "ALT" "GEN[0].QA" "EFF[0].GENE" "EFF[0].HGVS" "AA" "EFF[0].IMPACT" "GEN[0].GT" "GEN[0].AO"  > NIMI105-Hs2_Hs3_common.tsv 



for i in `ls *FLT3.vcf`
	do
	/home/gbucci/.conda/envs/gatk-3.6/bin/perl /home/gbucci/vep/variant_effect_predictor.pl  \
	-i ${i} --vcf -o  ${i%%.vcf}.vep.vcf --everything --assembly GRCh37 --cache_version 90 --cache --offline  \
	--fasta /home/gbucci/.vep/homo_sapiens/90_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa --force_overwrite -fork 4
done 


cd /lustre2/scratch/gbucci/Vago/160/Haloplex/HS1HS2/FASTQ/BBDUK/PINDEL

for i in `ls *FLT3.vep.vcf`
do
	/lustre2/scratch/gbucci/Vago/160/Haloplex/May_2018/VCF_MAF_ANNOTATE.sh $i
done
	


/home/gbucci/.conda/envs/gatk-3.6/bin/perl /home/gbucci/vep/variant_effect_predictor.pl  -i All.FB.sorted.vcf.gz --vcf -o  All.FB.sorted.vep.vcf --everything --assembly GRCh37 --cache_version 90 --cache --offline  --fasta /home/gbucci/.vep/homo_sapiens/90_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa --force_overwrite -fork 4
