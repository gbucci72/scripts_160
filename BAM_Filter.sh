#PBS -l select=1:app=java:ncpus=2:mem=6gb
#PBS -P 160 
#PBS -V
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
i=$arg1
name=${i%%.bam}

bamtools filter -tag "NM":"<8" -in ${i}| samtools view -b -F 0x04 -q 1 -| samtools sort  -T ${i%%.bam}.tmp -@ 2 - -o ${i%%.bam}_filtered.bam 

samtools index ${i%%.bam}_filtered.bam 

java -Xmx12G -jar /lustre1/tools/bin/GenomeAnalysisTK-2.1.8.jar \
-T ClipReads \
-R /lustre1/genomes/hg19/fa/hg19.fa \
-o ${i%%.bam}_filtered_clipped.bam \
-I ${i%%.bam}_filtered.bam  \
-CT "1-5,245-250" \
-QT 10 \
-CR WRITE_Q0S 


mv ${i%%.bam}_filtered_clipped.bai  ${i%%.bam}_filtered_clipped.bam.bai
