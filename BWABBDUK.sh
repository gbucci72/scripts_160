#!/bin/bash

#bwamem.sh filename.R1.fastq.gz reference
R1=$1
reference=$2
target=$3
#ref_genome=/lustre1/genomes/hg19/bwa/hg19
ref_genome=/lustre1/genomes/hg19/bwa/${reference}
fasta_genome=/lustre1/genomes/hg19/fa/${reference}.fa
#if [ ! -f ${ref_genome}.fa ]; then
#echo "$reference not found"
#exit
#fi

name=$R1
name=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level1; done)`
#lane=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level3; done)`
lane=1
echo "$name"
echo "$ref_genome"

user=`whoami`

R2=${R1/R1.duk/R2.duk}
 
BWA=/usr/local/cluster/bin/bwa
SAM=/usr/local/cluster/bin/samtools

script_name=bwa_${name:0:5}


cat <<__EOF__> $script_name
#PBS -N ${name}
#PBS -l select=1:app=java:ncpus=4:mem=8gb
#PBS -P 160 
#PBS -q workq
#PBS -m ae

cd $PWD

ulimit -l unlimited
ulimit -s unlimited

/usr/bin/time $BWA mem -t 3 -M -R "@RG\tID:$name\tPL:illumina\tPU:$lane\tSM:$name\tLB:$name\tCN:CTGB\tSO:coordinate" $ref_genome $R1 $R2| $SAM view -Su - | $SAM sort -T ${name}.tmp -@ 4 - -o ${name}_${reference}.bam
$SAM index ${name}_${reference}.bam

__EOF__

qsub $script_name
