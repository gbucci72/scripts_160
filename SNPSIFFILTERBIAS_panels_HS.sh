#PBS -l select=1:app=java:ncpus=4:mem=8gb
#PBS -P 160 
#PBS -V
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1

if [[ $targ != *.gz ]]
	then
		bgzip -f $targ
		echo "$targ bgzipped"
	else
		echo "$targ already zipped"
fi

zcat $targ |vcfbreakmulti| vcfsort| java -jar /usr/local/cluster/src/snpEff/SnpSift.jar filter \
"( DP > 4 ) & ( MQM[ANY] > 20 & MQMR[ANY] > 20 ) &  \
((RPL[ANY] > 1) | (RPR[ANY] > 1)) & \
(SAF[ANY] > 0) & (SAR[ANY] > 0)"  | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar filter \
"(( GEN[ANY].DP[ANY] > 4 ) & (GEN[ANY].AO[ANY] >= 2) & ( GEN[ANY].QA[ANY] > 30 ))" | \
bgzip -fc > ${targ%%.vcf*.gz}.multi.unBias.Qual.vcf.gz

tabix -f ${targ%%.vcf.gz}.multi.unBias.Qual.vcf.gz

###for i in `ls Merged.chr*.vcf.gz`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/SNPSIFTANNO.sh ; done
###for i in `ls *vcfx.gz`; do qsub -v arg1="$i" -N ${i%%.vcfx.gz} /lustre2/scratch/Vago/160/Haloplex/scripts_160/SNPSIFFILTERBIAS_panels_HS.sh; done