#PBS -l select=1:app=java:ncpus=1:mem=6gb
#PBS -P 160 
#PBS -V
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
FILE=$arg1
name=${i%%.bam}

AMP=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Covered.bed
FA=/lustre1/genomes/hg19/fa/hg19.fa

samtools mpileup  -d 1000 -Q 0 -l $AMP -f $FA -v -u -t DP,DV --ff DUP $FILE |bgzip -c > ${FILE%%.bam}.pileup.vcf.gz; tabix ${FILE%%.bam}.pileup.vcf.gz

