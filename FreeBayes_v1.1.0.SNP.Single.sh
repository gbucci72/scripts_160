#!/bin/bash

bam=$1
#region=$2

script_name=${bam%%.bam}.sh 

cat <<__EOF__> $script_name
HOST=`hostname`
echo $HOST

#PBS -N ${script_name}
#PBS -l select=1:ncpus=2:mem=6g:app=java
#PBS -o ${name}_${HOST}.log
#PBS -e ${name}_${HOST}.err
#PBS -q workq
#PBS -M bucci.gabriele@hsr.it
#PBS -m ae
#PBS -Wsandbox=private


cd $PWD

ulimit -l unlimited
ulimit -s unlimited

condactivate freebayes

freebayes -f /lustre1/genomes/hg19/fa/hg19.fa -l -@ Target.vcf.gz \
-b ${bam} -r chr9:139565149-139565151 -v ${bam%%.bam}_rs4636297.vcf


bgzip -f ${bam%%.bam}_rs4636297.vcf; tabix -fp vcf ${bam%%.bam}_rs4636297.vcf.gz

condeactivate

__EOF__

qsub $script_name