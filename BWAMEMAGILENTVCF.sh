#!/bin/bash

#bwamem.sh filename.R1.fastq.gz reference
R1=$1
reference=$2
target=$3
#ref_genome=/lustre1/genomes/hg19/bwa/hg19
ref_genome=/lustre1/genomes/hg19/bwa/${reference}
fasta_genome=/lustre1/genomes/hg19/fa/${reference}.fa
#if [ ! -f ${ref_genome}.fa ]; then
#echo "$reference not found"
#exit
#fi

name=$R1
name=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level1; done)`
#lane=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level3; done)`
lane=1
echo "$name"
echo "$ref_genome"

user=`whoami`

R2=${R1/_trimmed_1/_trimmed_2}
 
BWA=/lustre1/tools/bin/bwa
SAM=/usr/local/cluster/bin/samtools

script_name=bwa_${name:0:5}


cat <<__EOF__> $script_name
HOST=`hostname`

#PBS -N ${name}
#PBS -l select=1:ncpus=4:mem=8g:app=java
#PBS -V
#PBS -o ${name}_${HOST}.log
#PBS -e ${name}_${HOST}.err
#PBS -q workq
#PBS -M bucci.gabriele@hsr.it
#PBS -m ae
#PBS -Wsandbox=private

cd $PWD
#HOST=`hostname`
ulimit -l unlimited
ulimit -s unlimited
echo $HOST
/usr/bin/time $BWA mem -t 4 -M -R "@RG\tID:$name\tPL:illumina\tPU:$lane\tSM:$name\tCN:CTGB\tSO:coordinate" $ref_genome $R1 $R2| /lustre1/tools/bin/samtools view -Su - | /lustre1/tools/bin/samtools sort - ${name}_${reference} 
$SAM index ${name}_${reference}.bam

/lustre1/tools/bin/freebayes --strict-vcf -t ${target} -b ${name}_${reference}.bam -v ${name}_${reference}.vcf -f ${fasta_genome} -4 --pooled-continuous  -F 0.01 -C 2 --report-genotype-likelihood-max -j

__EOF__

qsub $script_name
