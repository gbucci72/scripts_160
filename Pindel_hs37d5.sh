#PBS -P 161
#PBS -V
#PBS -q workq
#PBS -l select=1:ncpus=4:mem=8g
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD
name=$arg1
NN=`ls *bam| wc -l`

pindel -f /lustre1/genomes/hs37d5/fa/hs37d5.fa -i config.txt -c $name -o ${name}_pindel -n 3 -M 2 -T 2 -x 1 

#ffor i in `cat /lustre1/genomes/hs37d5/annotation/hs37d5_chr_list`;do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/Pindel.sh ; done
