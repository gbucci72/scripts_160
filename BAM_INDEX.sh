bamFile=$1
name=${bamFile%%.bam}
script_name=${name}_index.sh
currentDirectory=`pwd`

cat <<__EOF__> $script_name

#!/bin/bash
#PBS -N I_${name:0:5}
#PBS -l select=1:ncpus=4:mem=8g:app=java
#PBS -Wsandbox=private
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it
#PBS -m ae


printf "%s\n" $bamFile

/usr/local/cluster/bin/samtools sort ${currentDirectory}/${bamFile} -T ${name}_tmp -o ${currentDirectory}/${name}_sorted.bam
/usr/local/cluster/bin/samtools index ${currentDirectory}/${name}_sorted.bam 

__EOF__

qsub $script_name





