#PBS -l select=1:app=java:ncpus=1:mem=2gb
#PBS -P 160 
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1

if [[ $targ != *.gz ]]
	then
		bgzip -f $targ
		echo "$targ bgzipped"
	else
		echo "$targ already zipped"
fi

bedtools intersect -v -header -a $targ -b /lustre1/genomes/hg19/annotation/LCR-hs37d5_chr.bed | bgzip -fc > ${targ%%.vcf.gz}.LCR.vcf.gz
tabix -f ${targ%%.vcf.gz}.LCR.vcf.gz

###for i in `ls Merged.chr*.vcf.gz`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/SNPSIFTANNO.sh ; done
###for i in `ls *multi.unBias.Qual.vcf.gz`; do qsub -v arg1="$i" -N ${i%%.multi.unBias.Qual.vcf.gz} /lustre2/scratch/Vago/160/Haloplex/scripts_160/SNPFILTERLCR.sh; done