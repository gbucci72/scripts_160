#!/bin/bash

#bwamem.sh filename.R1.fastq.gz reference
R1=$1
reference=$2
target=$3
#ref_genome=/lustre1/genomes/hg19/bwa/hg19
ref_genome=/lustre1/genomes/${reference}/bwa/${reference}
fasta_genome=/lustre1/genomes/${reference}/fa/${reference}.fa
#if [ ! -f ${ref_genome}.fa ]; then
#echo "$reference not found"
#exit
#fi

File=$R1
ID=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level1; done)`
library=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level2; done)`
lane=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level3; done)`
name=${ID}_${library}_${lane}
echo "$name"
echo "$ref_genome"

user=`whoami`

R2=${R1/R1.duk/R2.duk}
 
BWA=/usr/local/cluster/bin/bwa
SAM=/usr/local/cluster/bin/samtools

script_name=bwa_${name}


cat <<__EOF__> $script_name
#PBS -N ${name%%_*}
#PBS -l select=1:app=java:ncpus=1:mem=8gb
#PBS -P 160 
#PBS -q workq
#PBS -m ae

cd $PWD

ulimit -l unlimited
ulimit -s unlimited

/usr/bin/time $BWA mem -t 1 -M -R "@RG\tID:$name\tPL:illumina\tPU:$lane\tSM:$name\tLB:$name\tCN:CTGB\tSO:coordinate" $ref_genome $R1 $R2| $SAM view -Su - | $SAM sort -T ${name}.tmp -@ 4 - -o ${name}_${reference}.bam
$SAM index ${name}_${reference}.bam

__EOF__

qsub $script_name
