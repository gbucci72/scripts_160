#PBS -l select=1:app=java:ncpus=1:mem=2gb
#PBS -P 160
#PBS -V
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1
name=$arg2

zcat $targ |snpSift varType - |  vcffilter -g "( ( AO / ( RO + AO ) ) > 0.02 ) & ( AO > 2 )"| snpSift filter "(GEN[ANY].AO >= 2)" | \
snpSift filter "(VARTYPE has 'DEL') | (VARTYPE has 'INS')|((VARTYPE has 'SNP')&(GEN[ANY].AO >= 2))|((VARTYPE has 'MNP')&(GEN[ANY].AO >= 2))"| bgzip -c > ${targ%%.vcf.gz}.tier1.vcf.gz


