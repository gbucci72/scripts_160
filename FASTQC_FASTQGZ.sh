bamFile=$1
script_name=stat
currentDirectory=`pwd`

cat <<__EOF__> $script_name

#!/bin/bash
#PBS -N BAM_Stats
#PBS -l select=1:ncpus=1:mem=2g:app=java
#PBS -V
#PBS -Wsandbox=private
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it
#PBS -m ae


printf "%s\n" $bamFile
/usr/local/cluster/bin/fastqc --noextract -q  -t 1 -a /lustre1/genomes/Illumina_Adapters/Adapters.tab ${currentDirectory}/${bamFile} 

__EOF__

qsub $script_name
