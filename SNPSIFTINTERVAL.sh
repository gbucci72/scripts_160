#PBS -l select=1:app=java:ncpus=1:mem=4gb
#PBS -P 160 
#PBS -V
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1
bed=$arg2
##isplit -l 120 -d Regions.bed

bedtools window -w 25 -u -header -a $targ -b $bed | vcfsort| vcfuniq | vcffixup - | bgzip -fc > ${targ%%.vcf.gz}.IN.vcf.gz
tabix -f ${targ%%.vcf.gz}.IN.vcf.gz


###FILE=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Covered.bed
###for i in `ls *.multi.unBias.Qual.LCR.vcf.gz`; do qsub -v arg1="$i",arg2="$FILE" -N ${i%%.multi.unBias.Qual.LCR.vcf.gz} /lustre2/scratch/Vago/160/Haloplex/scripts_160/SNPSIFTINTERVAL.sh; done