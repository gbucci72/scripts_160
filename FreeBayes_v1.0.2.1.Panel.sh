#PBS -l select=1:app=java:ncpus=4:mem=8gb
#PBS -P 161 
#PBS -V
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
chunk=$arg1
##isplit -l 120 -d Regions.bed

/lustre1/tools/bin/freebayes -f /lustre1/genomes/hg19/fa/hg19.fa \
-F 0.01 -C 2 -4 -j --strict-vcf \
-@ All.Clean.vcf.gz -l \
--pooled-continuous --use-mapping-quality \
--genotype-qualities --report-genotype-likelihood-max \
-L BAMLIST.txt -t $chunk -v  ${chunk}.A.vcf


bgzip -f ${chunk}.A.vcf; tabix -fp vcf ${chunk}.A.vcf.gz

###for i in `ls x??`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/FreeBayes.sh ; done
