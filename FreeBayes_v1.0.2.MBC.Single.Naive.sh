#PBS -l select=1:app=java:ncpus=2:mem=6gb
#PBS -P 160 
#PBS -V
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
name=$arg1
chunk=$arg2
##isplit -l 120 -d Regions.bed

/lustre1/tools/bin/freebayes -f /lustre1/genomes/hg19/fa/hg19.fa \
-F 0.01 -C 2 -k -w -V -a  --strict-vcf \
--pooled-continuous  \
--min-base-quality 20 --min-mapping-quality 1 \
--min-repeat-entropy 1 --haplotype-length 0 \
-v  ${name%%.bam}_${chunk}.vcf -r $chunk $name


bgzip -f ${name%%.bam}_${chunk}.vcf; tabix -fp vcf ${name%%.bam}_${chunk}.vcf.gz

###for f in chr{1..22} chr{X,Y,M}; do qsub -v arg1="$f" -N ${i:0:6} ~/PBSScripts/FreeBayes.sh   ; done 
