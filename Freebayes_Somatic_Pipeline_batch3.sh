#New Freebayes Somatic
DEIV: 1 pretx, 2 relapse post auto, 2b relapse post allo, 3 germline, 4 donor
LUAN:1 pretx, 2 relapse post 1'allo, 2b relapse post 2'allo, 3 germline, 4 primo donor, 4b secondo donor

DIR=/lustre2/scratch/Vago/161/Exome/2017/POOL3/BAM

#GERMLINE call 5 exomes
#ALFE DEIV DR4 DR5 LUAN
#DEIV due relapse con autotrapianto
#LUAN due relapse, due donor


cd $DIR

cd $DIR
#make windows and chunks
bedtools makewindows -s 3 -w 20000 -g /lustre1/genomes/hg38/annotation/hg38.tab > hg38_main_200kw.bed
split -a 3 -l 10 hg38_main_200kw.bed -d chunk
#this generates 16 chunks

find /lustre2/scratch/Vago/161/Exome/2017/BAM -name "*bam" |sort -u > BAMLIST.txt
#the file has been edited and is not sorted

for i in `ls chunk???`; do /lustre2/scratch/Vago/161/Exome/2017/vago_161/FreeBayes_v1.0.2.sh $i; done   

###################################################################################################################
#QUALITY FILTERING BLOCK

cd VCF

for i in `ls *vcf`; do bgzip $i; tabix ${i}.gz; done &

#HARD FILTER 669aa34..a61954
for i in `ls *pre.vcf.gz`; do /lustre2/scratch/Vago/161/Exome/2017/vago_161/VCF_HARDFILTER_chunk.sh $i; done
zcat *pre.QUAL.vcf.gz| grep -c -v ‘#'
#585260
#Suspicious
for i in `ls *pre.QUAL.vcf.gz`; do /lustre2/scratch/Vago/161/Exome/2017/vago_161/VCF_QUALFILTER_chunk.sh $i; done
#MERGE OPTIONAL
export PERL5LIB=/usr/local/cluster/bin/
vcf-merge chunk???.pre.QUAL.S.vcf.gz |vcfsort | vcfuniq| vcffixup - | pbgzip -fc >  Combined.pre.QUAL.S.vcf.gz 
tabix -f Combined.pre.QUAL.S.vcf.gz 

###################################################################################################################
#SPLIT BY FAMILY 
#modifico lo script  caec049..532a01a
cat Family.txt |while read i; do echo $i; for z in `ls chunk???.pre.QUAL.S.vcf.gz`;do /lustre2/scratch/Vago/161/Exome/2017/vago_161/Split_by_family.sh $i $z ; done ; done
###################################################################################################################
#CONCAT + NORM + BREAKMULTI
#riunisco i chunk per famiglia

cat Family.txt |while read i; do echo $i; /lustre2/scratch/Vago/161/Exome/2017/vago_161/Family_merge_VCF.sh  $i ; done

###CHECK NPM###
cat Family.txt|while read i
do
	#echo $i
	vcfsamplenames All.${i}.vcf.gz
	zcat All.${i}.vcf.gz| snpSift filter "(CHROM='chr5')&((POS>171410530)&(POS<171410550)"|\
	vcfbreakmulti|bcftools norm -f /lustre2/scratch/gbucci/Genome/hs38DH.fa |vcfsort | vcfuniq| vcffixup -|\
	snpSift extractFields -e "." - "CHROM" "POS" "REF" "ALT" "GEN[*].GT"
done
###################################################################################################################
#ADD PEDIGREE HEADER and CALL SOMATIC (VCFSAMPLEDIFF)
cd /lustre2/scratch/Vago/161/Exome/2017/VCF/FILTERED
cd /lustre2/scratch/Vago/161/Exome/2017/POOL3/BAM/VCF

for i in {ALFE,DR4,DR5}
do
     zcat All.${i}.vcf.gz| \
     sed  '4i ##PEDIGREE=<Derived='"$i"'1,Original='"${i}"'3>' - | sed  '5i ##PEDIGREE=<Derived='"$i"'2,Original='"${i}"'3>' - | \
     vcfsamplediff preTx ${i}3 ${i}1 - | vcfsamplediff postTx ${i}3 ${i}2 - | \
     vcfsamplediff Donor ${i}4 ${i}2 - | \
     pbgzip -cf > ${i}.Diff.vcf.gz
     tabix -f ${i}.Diff.vcf.gz
done


i=LUAN
     zcat All.${i}.vcf.gz| \
     sed  '4i ##PEDIGREE=<Derived='"$i"'1,Original='"${i}"'3>' - | sed  '5i ##PEDIGREE=<Derived='"$i"'2,Original='"${i}"'3>' - | sed  '6i ##PEDIGREE=<Derived='"$i"'2b,Original='"${i}"'3>' - | \
     vcfsamplediff preTx ${i}3 ${i}1 - | vcfsamplediff postTx ${i}3 ${i}2 - | vcfsamplediff postTxb ${i}3 ${i}2b - |\
     vcfsamplediff Donor ${i}4 ${i}2 - | vcfsamplediff Donorb ${i}4b ${i}2b - |\
     pbgzip -cf > ${i}.Diff.vcf.gz
     tabix -f ${i}.Diff.vcf.gz

i=DEIV
     zcat All.${i}.vcf.gz| \
     sed  '4i ##PEDIGREE=<Derived='"$i"'1,Original='"${i}"'3>' - | sed  '5i ##PEDIGREE=<Derived='"$i"'2,Original='"${i}"'3>' - | sed  '6i ##PEDIGREE=<Derived='"$i"'2b,Original='"${i}"'3>' - |\
     vcfsamplediff preTx ${i}3 ${i}1 - | vcfsamplediff postTx ${i}3 ${i}2 - | vcfsamplediff postTxb ${i}3 ${i}2b - |\
     vcfsamplediff Donor ${i}4 ${i}2b - |\
     pbgzip -cf > ${i}.Diff.vcf.gz
     tabix -f ${i}.Diff.vcf.gz
###################################################################################################################

############################
#FILTER SOMATIC GL SPEESEQ #
############################
condeactivate

for i in {ALFE,DR4,DR5}
do
    vcftools --gzvcf ${i}.Diff.vcf.gz --indv ${i}1 --indv ${i}2 --indv ${i}3 --indv ${i}4 --out ${i} --recode
    sed -i 's/Number=R/Number=./g' ${i}.recode.vcf
    bgzip -f ${i}.recode.vcf
    tabix -f ${i}.recode.vcf.gz
done

###QUELLI CON DOPPIA RELAPSE

for i in LUAN
do
    vcftools --gzvcf ${i}.Diff.vcf.gz --indv ${i}1 --indv ${i}2 --indv ${i}2b --indv ${i}3 --indv ${i}4 --indv ${i}4b --out ${i} --recode
    sed -i 's/Number=R/Number=./g' ${i}.recode.vcf
    bgzip -f ${i}.recode.vcf
    tabix -f ${i}.recode.vcf.gz
done &

for i in DEIV
do
    vcftools --gzvcf ${i}.Diff.vcf.gz --indv ${i}1 --indv ${i}2 --indv ${i}2b --indv ${i}3 --indv ${i}4  --out ${i} --recode
    sed -i 's/Number=R/Number=./g' ${i}.recode.vcf
    bgzip -f ${i}.recode.vcf
    tabix -f ${i}.recode.vcf.gz
done &
#SPEESEQ

for NAME in {ALFE,DR4,DR5}
do  
	python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}1 --normal ${NAME}3 | vcffixup - > ${NAME}1_${NAME}3_Somatic.vcf &
	python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2 --normal ${NAME}3 | vcffixup - > ${NAME}2_${NAME}3_Somatic.vcf &
	python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2 --normal ${NAME}4 | vcffixup - > ${NAME}2_${NAME}4_Somatic.vcf &
	python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}3 --normal ${NAME}4 | vcffixup - > ${NAME}3_${NAME}4_Somatic.vcf &
done


NAME=LUAN; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}1 --normal ${NAME}3 | vcffixup - > ${NAME}1_${NAME}3_Somatic.vcf ;
NAME=LUAN; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2 --normal ${NAME}3 | vcffixup - > ${NAME}2_${NAME}3_Somatic.vcf ;
NAME=LUAN; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2 --normal ${NAME}4 | vcffixup - > ${NAME}2_${NAME}4_Somatic.vcf ;
NAME=LUAN; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2b --normal ${NAME}3 | vcffixup - > ${NAME}2b_${NAME}3_Somatic.vcf ;
NAME=LUAN; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2b --normal ${NAME}4b | vcffixup - > ${NAME}2b_${NAME}4b_Somatic.vcf ;
NAME=LUAN; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2b --normal ${NAME}3 | vcffixup - > ${NAME}2b_${NAME}3_Somatic.vcf
NAME=LUAN; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}3 --normal ${NAME}4 | vcffixup - > ${NAME}3_${NAME}4_Somatic.vcf
NAME=LUAN; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}3 --normal ${NAME}4b | vcffixup - > ${NAME}3_${NAME}4b_Somatic.vcf

NAME=DEIV ; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}1 --normal ${NAME}3 | vcffixup - > ${NAME}1_${NAME}3_Somatic.vcf ;
NAME=DEIV ; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2 --normal ${NAME}3 | vcffixup - > ${NAME}2_${NAME}3_Somatic.vcf ;
NAME=DEIV ; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2b --normal ${NAME}3 | vcffixup - > ${NAME}2b_${NAME}3_Somatic.vcf ;
NAME=DEIV ; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2b --normal ${NAME}4 | vcffixup - > ${NAME}2b_${NAME}4_Somatic.vcf ;
NAME=DEIV ; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}2b --normal ${NAME}3 | vcffixup - > ${NAME}2b_${NAME}3_Somatic.vcf
NAME=DEIV ; python2.7  /lustre2/scratch/filter_somatic_FB.py --vcf  ${NAME}.recode.vcf.gz --tumor ${NAME}3 --normal ${NAME}4 | vcffixup - > ${NAME}3_${NAME}4_Somatic.vcf

for i in `ls *_*_Somatic.vcf`; do bgzip -f $i;tabix -f ${i}.gz; done
for i in `ls *_Somatic.vcf.gz`; do echo $i.;zgrep -c -v "#" $i;done

# for i in `ls *_Somatic.vcf.gz`
# do
# 	echo $i
# 	vcfsamplenames ${i}
# 	zcat ${i}| snpSift filter "(CHROM='chr5')&((POS>171410530)&(POS<171410550)"|\
# 	vcfbreakmulti|bcftools norm -f /lustre2/scratch/gbucci/Genome/hs38DH.fa |vcfsort | vcfuniq| vcffixup -|\
# 	snpSift extractFields -e "." - "CHROM" "POS" "REF" "ALT" "GEN[*].GT"
# done
###################################################################################################################
#Annoto
for i in `ls *Somatic.vcf.gz`; do /lustre2/scratch/Vago/161/Exome/2017/vago_161/SNPSIFTSNPEFF_family.sh $i; done


###################################################################################################################
#Intersect with CNV

for i in LUAN; 
do
    cat  ${i}1.Diff.call.cns | grep -v "^chr.*_"|awk -v a=${i}1 ' NR > 1 {FS="\t";OFS="\t";print $1,$2,$3,$7,$7}' |bedtools sort > ${i}1.CNA.bed
    cat  ${i}2.Diff.call.cns | grep -v "^chr.*_"|awk -v a=${i}2 ' NR > 1 {FS="\t";OFS="\t";print $1,$2,$3,$7,$7}' |bedtools sort > ${i}2.CNA.bed
    cat  ${i}2b*.Diff.call.cns | grep -v "^chr.*_"|awk -v a=${i}2b ' NR > 1 {FS="\t";OFS="\t";print $1,$2,$3,$7,$7}' |bedtools sort > ${i}2b.CNA.bed
done

for i in DEIV; 
do
	cat  ${i}1.Diff.call.cns | grep -v "^chr.*_"|awk -v a=${i}1 ' NR > 1 {FS="\t";OFS="\t";print $1,$2,$3,$7,$7}' |bedtools sort > ${i}1.CNA.bed
	cat  ${i}2.Diff.call.cns | grep -v "^chr.*_"|awk -v a=${i}2 ' NR > 1 {FS="\t";OFS="\t";print $1,$2,$3,$7,$7}' |bedtools sort > ${i}2.CNA.bed
	cat  ${i}2b*.Diff.call.cns | grep -v "^chr.*_"|awk -v a=${i}2b ' NR > 1 {FS="\t";OFS="\t";print $1,$2,$3,$7,$7}' |bedtools sort > ${i}2b.CNA.bed
done

for i in {ALFE,DR4,DR5}
do
	cat  ${i}1.Diff.call.cns |grep -v "^chr.*_"| awk -v a=${i}1 ' NR > 1 {FS="\t";OFS="\t";print $1,$2,$3,$7,$7}' |bedtools sort > ${i}1.CNA.bed
	cat  ${i}2.Diff.call.cns |grep -v "^chr.*_"| awk -v a=${i}2 ' NR > 1 {FS="\t";OFS="\t";print $1,$2,$3,$7,$7}' |bedtools sort > ${i}2.CNA.bed
done

for i in `ls *_Somatic.Eff.vcf.gz`
do
	A=${i%%_Somatic.Eff.vcf.gz}
	B=${A%%_*}
	vcfannotate -b ${B}.CNA.bed -k PLOIDY -d 2 $i > ${i%%_Somatic.Eff.vcf.gz}_Somatic.Anno.vcf
done

for i in `ls *_Somatic.Anno.vcf`
do
	bgzip -f ${i}; tabix -f ${i}.gz
done
#FILTER ON PASS SOMATIC AND non synonymus
#FILTER

for i in `ls *_Somatic.Anno.vcf.gz`
do

	zcat $i | snpSift filter "(na FILTER) | (FILTER = 'PASS')" |\
	snpSift filter "(EFF[0].EFFECT != 'synonymous_variant')" > ${i%%_Somatic.Anno.vcf.gz}_Somatic.Anno.SPEE.vcf
done 

for i in `ls *_Somatic.Anno.SPEE.vcf`
do
	bgzip -f ${i}; tabix -f ${i}.gz
done
#ESTRAGGO TABELLE

for i in `ls {ALFE,DR4,DR5}*_Somatic.Anno.SPEE.vcf.gz`
do
     A=${i%%_Somatic.Anno.SPEE.vcf.gz}
     readarray C < <(vcfsamplenames $i|tee)
     B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B3=`echo ${C[2]}`;B4=`echo ${C[3]}`
     zcat ${i} |snpSift extractFields -e "." - \
     "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
     "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
     "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
     "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
     "GEN[${B1}].GT" "GEN[${B2}].GT" "GEN[${B3}].GT" "GEN[${B4}].GT" \
     "GEN[${B1}].RO" "GEN[${B2}].RO" "GEN[${B3}].RO" "GEN[${B4}].RO"     \
     "GEN[${B1}].AO" "GEN[${B2}].AO" "GEN[${B3}].AO" "GEN[${B4}].AO" \
     "GEN[${B1}].QA" "GEN[${B2}].QA" "GEN[${B3}].QA" "GEN[${B4}].QA" \
     |sed -e "s/1\/1/'1\/1/g" > ${i%%_Somatic.Anno.SPEE.vcf.gz}_Somatic.Anno.SPEE.tsv
done

for i in `ls LUAN*_Somatic.Anno.SPEE.vcf.gz`
do
     A=${i%%_Somatic.Anno.SPEE.vcf.gz}
     readarray C < <(vcfsamplenames $i|tee)
     B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B2b=`echo ${C[2]}`;B3=`echo ${C[3]}`;B4=`echo ${C[4]}`;B5=`echo ${C[5]}`;
     zcat ${i} |snpSift extractFields -e "." - \
     "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
     "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
     "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
     "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
     "GEN[${B1}].GT" "GEN[${B2}].GT" "GEN[${B2b}].GT" "GEN[${B3}].GT" "GEN[${B4}].GT" "GEN[${B5}].GT"\
     "GEN[${B1}].RO" "GEN[${B2}].RO" "GEN[${B2b}].RO" "GEN[${B3}].RO" "GEN[${B4}].RO" "GEN[${B5}].RO"    \
     "GEN[${B1}].AO" "GEN[${B2}].AO" "GEN[${B2b}].AO" "GEN[${B3}].AO" "GEN[${B4}].AO" "GEN[${B5}].AO" \
     "GEN[${B1}].QA" "GEN[${B2}].QA" "GEN[${B2b}].QA" "GEN[${B3}].QA" "GEN[${B4}].QA" "GEN[${B5}].QA" \
     |sed -e "s/1\/1/'1\/1/g" > ${i%%_Somatic.Anno.SPEE.vcf.gz}_Somatic.Anno.SPEE.tsv
done


for i in `ls DEIV*_Somatic.Anno.SPEE.vcf.gz`
do
     A=${i%%_Somatic.Anno.SPEE.vcf.gz}
     readarray C < <(vcfsamplenames $i|tee)
     B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B2b=`echo ${C[2]}`;B3=`echo ${C[3]}`;B4=`echo ${C[4]}`
     zcat ${i} |snpSift extractFields -e "." - \
     "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
     "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
     "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
     "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
     "GEN[${B1}].GT" "GEN[${B2}].GT" "GEN[${B2b}].GT" "GEN[${B3}].GT" "GEN[${B4}].GT" \
     "GEN[${B1}].RO" "GEN[${B2}].RO" "GEN[${B2b}].RO" "GEN[${B3}].RO" "GEN[${B4}].RO"     \
     "GEN[${B1}].AO" "GEN[${B2}].AO" "GEN[${B2b}].AO" "GEN[${B3}].AO" "GEN[${B4}].AO" \
     "GEN[${B1}].QA" "GEN[${B2}].QA" "GEN[${B2b}].QA" "GEN[${B3}].QA" "GEN[${B4}].QA" \
     |sed -e "s/1\/1/'1\/1/g" > ${i%%_Somatic.Anno.SPEE.vcf.gz}_Somatic.Anno.SPEE.tsv
done
###################################################################################################################	 
###################################################################################################################
#MERGE pre and Post
###################################################################################################################
###################################################################################################################
	
###UNFILTERED####
for i in `ls {ALFE,DR4,DR5}*_Somatic.Anno.vcf.gz`
do
	 A=${i%%_Somatic.Anno.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B3=`echo ${C[2]}`;B4=`echo ${C[3]}`
	 echo $B1 $B2 $B3 $B4
	 vcfkeepsamples ${B1}_${B3}_Somatic.Anno.vcf.gz $B1 > $B1_Somatic.Anno.vcf
	 vcfkeepsamples ${B2}_${B3}_Somatic.Anno.vcf.gz $B2 > $B2_Somatic.Anno.vcf
	 vcfkeepsamples ${B3}_${B4}_Somatic.Anno.vcf.gz $B3 > $B3_Somatic.Anno.vcf
	 #vcfkeepsample ${B1}_${B3}_Somatic.Anno.vcf.gz $B1 > $B1_Somatic.Anno.vcf.gz
	 vcf-concat ${B1}_${B3}_Somatic.Anno.vcf.gz ${B2}_${B3}_Somatic.Anno.vcf.gz | vcfbreakmulti|bcftools norm -f /lustre2/scratch/gbucci/Genome/hs38DH.fa |\
	 vcfsort | vcfuniq| vcffixup - | bgzip -fc > ${B1}_${B2}_Somatic.Anno.vcf.gz
	 tabix -f ${B1}_${B2}_Somatic.Anno.vcf.gz
done

for i in `ls LUAN1*_Somatic.Anno.vcf.gz`
do
	 A=${i%%_Somatic.Anno.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B2b=`echo ${C[2]}`;B3=`echo ${C[3]}`;B4=`echo ${C[4]}`;B5=`echo ${C[5]}`;
	 echo $B1 $B2 $B3 $B4 $B5 $B6
	 vcfkeepsamples ${B1}_${B3}_Somatic.Anno.vcf.gz $B1 > $B1_Somatic.Anno.vcf
	 vcfkeepsamples ${B2}_${B3}_Somatic.Anno.vcf.gz $B2 > $B2_Somatic.Anno.vcf
	 vcfkeepsamples ${B3}_${B4}_Somatic.Anno.vcf.gz $B3 > $B3_Somatic.Anno.vcf
	 vcfkeepsample ${B2b}_${B3}_Somatic.Anno.vcf.gz $B2b > $B2b_Somatic.Anno.vcf.gz
	 vcf-concat ${B1}_${B3}_Somatic.Anno.vcf.gz ${B2}_${B3}_Somatic.Anno.vcf.gz ${B2b}_${B3}_Somatic.Anno.vcf.gz| vcfbreakmulti|bcftools norm -f /lustre2/scratch/gbucci/Genome/hs38DH.fa |\
	 vcfsort | vcfuniq| vcffixup - | bgzip -fc > ${B1}_${B2}_${B2b}_Somatic.Anno.vcf.gz
	 tabix -f ${B1}_${B2}_${B2b}_Somatic.Anno.vcf.gz
done

for i in `ls DEIV1*_Somatic.Anno.vcf.gz`
do
	 A=${i%%_Somatic.Anno.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B2b=`echo ${C[2]}`;B3=`echo ${C[3]}`;B4=`echo ${C[4]}`
	 echo $B1 $B2 $B3 $B4 $B5 $B6
	 vcfkeepsamples ${B1}_${B3}_Somatic.Anno.vcf.gz $B1 > $B1_Somatic.Anno.vcf
	 vcfkeepsamples ${B2}_${B3}_Somatic.Anno.vcf.gz $B2 > $B2_Somatic.Anno.vcf
	 vcfkeepsamples ${B3}_${B4}_Somatic.Anno.vcf.gz $B3 > $B3_Somatic.Anno.vcf
	 vcfkeepsample ${B2b}_${B3}_Somatic.Anno.vcf.gz $B2b > $B2b_Somatic.Anno.vcf.gz
	 vcf-concat ${B1}_${B3}_Somatic.Anno.vcf.gz ${B2}_${B3}_Somatic.Anno.vcf.gz ${B2b}_${B3}_Somatic.Anno.vcf.gz| vcfbreakmulti|bcftools norm -f /lustre2/scratch/gbucci/Genome/hs38DH.fa |\
	 vcfsort | vcfuniq| vcffixup - | bgzip -fc > ${B1}_${B2}_${B2b}_Somatic.Anno.vcf.gz
	 tabix -f ${B1}_${B2}_${B2b}_Somatic.Anno.vcf.gz
done

#ESTRAGGO TABELLE

for i in `ls {ALFE,DR4,DR5}*_Somatic.Anno.vcf.gz`
do
	 A=${i%%_Somatic.Anno.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B3=`echo ${C[2]}`;B4=`echo ${C[3]}`
	 zcat ${i} |snpSift extractFields -e "." - \
	 "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
	 "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
	 "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
	 "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
	 "GEN[${B1}].GT" "GEN[${B2}].GT" "GEN[${B3}].GT" "GEN[${B4}].GT" \
	 "GEN[${B1}].RO" "GEN[${B2}].RO" "GEN[${B3}].RO" "GEN[${B4}].RO"     \
	 "GEN[${B1}].AO" "GEN[${B2}].AO" "GEN[${B3}].AO" "GEN[${B4}].AO" \
	 "GEN[${B1}].QA" "GEN[${B2}].QA" "GEN[${B3}].QA" "GEN[${B4}].QA" \
	 |sed -e "s/1\/1/'1\/1/g" > ${i%%_Somatic.Anno.vcf.gz}_Somatic.Anno.tsv
done

for i in `ls LUAN*_Somatic.Anno.vcf.gz`
do
	 A=${i%%_Somatic.Anno.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B2b=`echo ${C[2]}`;B3=`echo ${C[3]}`;B4=`echo ${C[4]}`;B5=`echo ${C[5]}`;
	 zcat ${i} |snpSift extractFields -e "." - \
	 "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
	 "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
	 "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
	 "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
	 "GEN[${B1}].GT" "GEN[${B2}].GT" "GEN[${B2b}].GT" "GEN[${B3}].GT" "GEN[${B4}].GT" "GEN[${B5}].GT"\
	 "GEN[${B1}].RO" "GEN[${B2}].RO" "GEN[${B2b}].RO" "GEN[${B3}].RO" "GEN[${B4}].RO" "GEN[${B5}].RO"    \
	 "GEN[${B1}].AO" "GEN[${B2}].AO" "GEN[${B2b}].AO" "GEN[${B3}].AO" "GEN[${B4}].AO" "GEN[${B5}].AO" \
	 "GEN[${B1}].QA" "GEN[${B2}].QA" "GEN[${B2b}].QA" "GEN[${B3}].QA" "GEN[${B4}].QA" "GEN[${B5}].QA" \
	 |sed -e "s/1\/1/'1\/1/g" > ${i%%_Somatic.Anno.vcf.gz}_Somatic.Anno.tsv
done


for i in `ls DEIV*_Somatic.Anno.vcf.gz`
do
	 A=${i%%_Somatic.Anno.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B2b=`echo ${C[2]}`;B3=`echo ${C[3]}`;B4=`echo ${C[4]}`
	 zcat ${i} |snpSift extractFields -e "." - \
	 "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
	 "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
	 "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
	 "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
	 "GEN[${B1}].GT" "GEN[${B2}].GT" "GEN[${B2b}].GT" "GEN[${B3}].GT" "GEN[${B4}].GT" \
	 "GEN[${B1}].RO" "GEN[${B2}].RO" "GEN[${B2b}].RO" "GEN[${B3}].RO" "GEN[${B4}].RO"     \
	 "GEN[${B1}].AO" "GEN[${B2}].AO" "GEN[${B2b}].AO" "GEN[${B3}].AO" "GEN[${B4}].AO" \
	 "GEN[${B1}].QA" "GEN[${B2}].QA" "GEN[${B2b}].QA" "GEN[${B3}].QA" "GEN[${B4}].QA" \
	 |sed -e "s/1\/1/'1\/1/g" > ${i%%_Somatic.Anno.vcf.gz}_Somatic.Anno.tsv
done
###UNFILTERED####
###################################################################################################################	
###FILTERED####

for i in `ls {ALFE,DR4,DR5}*_Somatic.Anno.SPEE.vcf.gz`
do
	 A=${i%%_Somatic.Anno.SPEE.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B3=`echo ${C[2]}`;B4=`echo ${C[3]}`
	 echo $B1 $B2 $B3 $B4
	 vcfkeepsamples ${B1}_${B3}_Somatic.Anno.SPEE.vcf.gz $B1 > $B1_Somatic.Anno.SPEE.vcf
	 vcfkeepsamples ${B2}_${B3}_Somatic.Anno.SPEE.vcf.gz $B2 > $B2_Somatic.Anno.SPEE.vcf
	 vcfkeepsamples ${B3}_${B4}_Somatic.Anno.SPEE.vcf.gz $B3 > $B3_Somatic.Anno.SPEE.vcf
	 #vcfkeepsample ${B1}_${B3}_Somatic.Anno.SPEE.vcf.gz $B1 > $B1_Somatic.Anno.SPEE.vcf.gz
	 vcf-concat ${B1}_${B3}_Somatic.Anno.SPEE.vcf.gz ${B2}_${B3}_Somatic.Anno.SPEE.vcf.gz | vcfbreakmulti|bcftools norm -f /lustre2/scratch/gbucci/Genome/hs38DH.fa |\
	 vcfsort | vcfuniq| vcffixup - | bgzip -fc > ${B1}_${B2}_Somatic.Anno.SPEE.vcf.gz
	 tabix -f ${B1}_${B2}_Somatic.Anno.SPEE.vcf.gz
done

for i in `ls LUAN1*_Somatic.Anno.SPEE.vcf.gz`
do
	 A=${i%%_Somatic.Anno.SPEE.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B2b=`echo ${C[2]}`;B3=`echo ${C[3]}`;B4=`echo ${C[4]}`;B5=`echo ${C[5]}`;
	 echo $B1 $B2 $B3 $B4 $B5 $B6
	 vcfkeepsamples ${B1}_${B3}_Somatic.Anno.SPEE.vcf.gz $B1 > $B1_Somatic.Anno.SPEE.vcf
	 vcfkeepsamples ${B2}_${B3}_Somatic.Anno.SPEE.vcf.gz $B2 > $B2_Somatic.Anno.SPEE.vcf
	 vcfkeepsamples ${B3}_${B4}_Somatic.Anno.SPEE.vcf.gz $B3 > $B3_Somatic.Anno.SPEE.vcf
	 vcfkeepsample ${B2b}_${B3}_Somatic.Anno.SPEE.vcf.gz $B2b > $B2b_Somatic.Anno.SPEE.vcf.gz
	 vcf-concat ${B1}_${B3}_Somatic.Anno.SPEE.vcf.gz ${B2}_${B3}_Somatic.Anno.SPEE.vcf.gz ${B2b}_${B3}_Somatic.Anno.SPEE.vcf.gz| vcfbreakmulti|bcftools norm -f /lustre2/scratch/gbucci/Genome/hs38DH.fa |\
	 vcfsort | vcfuniq| vcffixup - | bgzip -fc > ${B1}_${B2}_${B2b}_Somatic.Anno.SPEE.vcf.gz
	 tabix -f ${B1}_${B2}_${B2b}_Somatic.Anno.SPEE.vcf.gz
done

for i in `ls DEIV1*_Somatic.Anno.SPEE.vcf.gz`
do
	 A=${i%%_Somatic.Anno.SPEE.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B2b=`echo ${C[2]}`;B3=`echo ${C[3]}`;B4=`echo ${C[4]}`
	 echo $B1 $B2 $B3 $B4 $B5 $B6
	 vcfkeepsamples ${B1}_${B3}_Somatic.Anno.SPEE.vcf.gz $B1 > $B1_Somatic.Anno.SPEE.vcf
	 vcfkeepsamples ${B2}_${B3}_Somatic.Anno.SPEE.vcf.gz $B2 > $B2_Somatic.Anno.SPEE.vcf
	 vcfkeepsamples ${B3}_${B4}_Somatic.Anno.SPEE.vcf.gz $B3 > $B3_Somatic.Anno.SPEE.vcf
	 vcfkeepsample ${B2b}_${B3}_Somatic.Anno.SPEE.vcf.gz $B2b > $B2b_Somatic.Anno.SPEE.vcf.gz
	 vcf-concat ${B1}_${B3}_Somatic.Anno.SPEE.vcf.gz ${B2}_${B3}_Somatic.Anno.SPEE.vcf.gz ${B2b}_${B3}_Somatic.Anno.SPEE.vcf.gz| vcfbreakmulti|bcftools norm -f /lustre2/scratch/gbucci/Genome/hs38DH.fa |\
	 vcfsort | vcfuniq| vcffixup - | bgzip -fc > ${B1}_${B2}_${B2b}_Somatic.Anno.SPEE.vcf.gz
	 tabix -f ${B1}_${B2}_${B2b}_Somatic.Anno.SPEE.vcf.gz
done

#ESTRAGGO TABELLE

for i in `ls {ALFE,DR4,DR5}*_Somatic.Anno.SPEE.vcf.gz`
do
	 A=${i%%_Somatic.Anno.SPEE.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B3=`echo ${C[2]}`;B4=`echo ${C[3]}`
	 zcat ${i} |snpSift extractFields -e "." - \
	 "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
	 "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
	 "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
	 "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
	 "GEN[${B1}].GT" "GEN[${B2}].GT" "GEN[${B3}].GT" "GEN[${B4}].GT" \
	 "GEN[${B1}].RO" "GEN[${B2}].RO" "GEN[${B3}].RO" "GEN[${B4}].RO"     \
	 "GEN[${B1}].AO" "GEN[${B2}].AO" "GEN[${B3}].AO" "GEN[${B4}].AO" \
	 "GEN[${B1}].QA" "GEN[${B2}].QA" "GEN[${B3}].QA" "GEN[${B4}].QA" \
	 |sed -e "s/1\/1/'1\/1/g" > ${i%%_Somatic.Anno.SPEE.vcf.gz}_Somatic.Anno.SPEE.tsv
done

for i in `ls LUAN*_Somatic.Anno.SPEE.vcf.gz`
do
	 A=${i%%_Somatic.Anno.SPEE.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B2b=`echo ${C[2]}`;B3=`echo ${C[3]}`;B4=`echo ${C[4]}`;B5=`echo ${C[5]}`;
	 zcat ${i} |snpSift extractFields -e "." - \
	 "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
	 "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
	 "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
	 "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
	 "GEN[${B1}].GT" "GEN[${B2}].GT" "GEN[${B2b}].GT" "GEN[${B3}].GT" "GEN[${B4}].GT" "GEN[${B5}].GT"\
	 "GEN[${B1}].RO" "GEN[${B2}].RO" "GEN[${B2b}].RO" "GEN[${B3}].RO" "GEN[${B4}].RO" "GEN[${B5}].RO"    \
	 "GEN[${B1}].AO" "GEN[${B2}].AO" "GEN[${B2b}].AO" "GEN[${B3}].AO" "GEN[${B4}].AO" "GEN[${B5}].AO" \
	 "GEN[${B1}].QA" "GEN[${B2}].QA" "GEN[${B2b}].QA" "GEN[${B3}].QA" "GEN[${B4}].QA" "GEN[${B5}].QA" \
	 |sed -e "s/1\/1/'1\/1/g" > ${i%%_Somatic.Anno.SPEE.vcf.gz}_Somatic.Anno.SPEE.tsv
done


for i in `ls DEIV*_Somatic.Anno.SPEE.vcf.gz`
do
	 A=${i%%_Somatic.Anno.SPEE.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B2b=`echo ${C[2]}`;B3=`echo ${C[3]}`;B4=`echo ${C[4]}`
	 zcat ${i} |snpSift extractFields -e "." - \
	 "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
	 "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
	 "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
	 "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
	 "GEN[${B1}].GT" "GEN[${B2}].GT" "GEN[${B2b}].GT" "GEN[${B3}].GT" "GEN[${B4}].GT" \
	 "GEN[${B1}].RO" "GEN[${B2}].RO" "GEN[${B2b}].RO" "GEN[${B3}].RO" "GEN[${B4}].RO"     \
	 "GEN[${B1}].AO" "GEN[${B2}].AO" "GEN[${B2b}].AO" "GEN[${B3}].AO" "GEN[${B4}].AO" \
	 "GEN[${B1}].QA" "GEN[${B2}].QA" "GEN[${B2b}].QA" "GEN[${B3}].QA" "GEN[${B4}].QA" \
	 |sed -e "s/1\/1/'1\/1/g" > ${i%%_Somatic.Anno.SPEE.vcf.gz}_Somatic.Anno.SPEE.tsv
done
###FILTERED####
###################################################################################################################
###################################################################################################################
#MLL
cd /lustre2/scratch/Vago/161/Exome/2017/POOL3/BAM
mkdir MLL
cd MLL
ln -s /lustre1/workspace/Ciceri/161_Leukemia/POOL3/*bam* .
for i in *bai; do ln -s ${i} ${i%%.bai}.bam.bai; done

zgrep KMT2A-001 /lustre1/genomes/hg38/annotation/gencode.v26.chr_patch_hapl_scaff.annotation.gtf| grep -w "exon_number 3"| awk '$3 =="exon" {OFS="\t";print $1,$4,$5,$3}' | sed 's/exon/exon3/g' > Exon3.bed
zgrep KMT2A-001 /lustre1/genomes/hg38/annotation/gencode.v26.chr_patch_hapl_scaff.annotation.gtf| grep -w "exon_number 27"| awk '$3 =="exon" {OFS="\t";print $1,$4,$5,$3}' |sed 's/exon/exon27/g'> Exon27.bed

cat Exon* | bedtools sort > MLL_Exon.bed 

ls *bam| sort| cut -d_ -f1 > Names.txt 

bedtools multicov -q 20 -bams `ls *bam| sort` -bed MLL_Exon.bed  > MLLexons.cov &


###################################################################################################################
#FLT3-ITD

mkdir /lustre2/scratch/Vago/161/Exome/2017/POOL3/BAM/PINDEL
cd  /lustre2/scratch/Vago/161/Exome/2017/POOL3/BAM/PINDEL

ln -s /lustre1/workspace/Ciceri/161_Leukemia/POOL3/*.ba? .

condactivate gatk-3.6

bamPEFragmentSize --histogram Sizes.png -p 4 --bamfiles *bam & #bisogna dare dei limit alla max(X) perche' il grafico e' schiacciato a sx 

for i in `ls *bam`; do printf "%s\t" $i; printf "%s\t" "150"; printf "%s\n" ${i%%_RGSorted.bam}; done > config.ini

pindel -f /lustre2/scratch/gbucci/Genome/hs38DH.fa -o ALL -c ALL -i config.ini -T 6 -x 3
pindel2vcf -r /lustre2/scratch/gbucci/Genome/hs38DH.fa -R GRCH38DH -d 20170620 -p ALL_SI --min_size 4 --max_size 20000 --min_supporting_reads 2 &


#pindel -f /lustre2/scratch/gbucci/Genome/hs38DH.fa -o chr13 -c chr13 -i config.ini -T 6 -x 3
#pindel2vcf -r /lustre2/scratch/gbucci/Genome/hs38DH.fa -R GRCH38DH -d 20170620 -p chr13_SI --min_size 4 --max_size 20000 --min_supporting_reads 2 &

#FLT3ITD="chr13:28033700-28034500"#region spanning exon 13 to 15
pindel  -f /lustre2/scratch/gbucci/Genome/hs38DH.fa -i config.ini -o FLT3 -L Pindel.log  -n 7 -M 2 -T 10 -x 3 -c chr13:28033700-28034500 &
pindel2vcf -r /lustre2/scratch/gbucci/Genome/hs38DH.fa -R GRCH38DH -d 20170620 -p FLT3_SI --min_size 4 --max_size 20000 --min_supporting_reads 2 &


bgzip  FLT3_SI.vcf
tabix FLT3_SI.vcf.gz
targ=FLT3_SI.vcf.gz

condactivate gatk-3.6

#zcat $targ |snpEff GRCh38.86 | vcfsort| bgzip -f -c > ${targ%%.vcf.gz}.Eff.vcf.gz & NOT WORKING
#bgzip -f FLT3_SI.Eff.vcf
#tabix -f FLT3_SI.Eff.vcf.gz

#snpSift extractFields FLT3_SI.Eff.vcf.gz "CHROM" "POS" "REF" "ALT" "HOMLEN" "EFF[0].GENE" "EFF[0].HGVS" "EFF[0].FEATUREID" "EFF[0].IMPACT" "EFF[0].EFFECT" "ANN[0].BIOTYPE" "GEN[*].AD" > FLT3_ITD.tsv

#USO VEP ONLINE
#e salvo in VCF
snpSift extractFields FLT3-ITD_VEP.vcf "CHROM" "POS" "REF" "ALT" "HOMLEN" "EFF[0].GENE" "EFF[0].HGVS" "EFF[0].FEATUREID" "EFF[0].IMPACT" "EFF[0].EFFECT" "ANN[0].BIOTYPE" "GEN[*].AD" > FLT3_ITD.tsv

#####COMPARISON
cd /lustre2/scratch/Vago/161/Exome/2017/REL_vs_DIAG_Somatic

for i in `ls *Somatic.Anno.vcf.gz`; do vcfglxgt $i| bgzip -c > ${i%%.vcf.gz}.GTFIX.vcf.gz; done
for i in `ls *GTFIX.vcf.gz`; do tabix $i; done

###UNFILTERED and GERMLINE####
for i in `ls {ALFE,DR4,DR5}1_Somatic.Anno.GTFIX.vcf.gz`
do
	 A=${i%%_Somatic.Anno.GTFIX.vcf.gz}
	 readarray C < <(vcfsamplenames $i|tee)
	 B1=`echo ${C[0]}`;B2=`echo ${C[1]}`;B3=`echo ${C[2]}`;B4=`echo ${C[3]}`
	 echo $B1 $B2 $B3 $B4
	 vcfkeepsamples ${B1}_${B3}_Somatic.Anno.GTFIX.vcf.gz $B1 | bgzip -c > ${B1}_Somatic.Anno.GTFIX.vcf.gz
	 vcfkeepsamples ${B2}_${B3}_Somatic.Anno.GTFIX.vcf.gz $B2 | bgzip -c > ${B2}_Somatic.Anno.GTFIX.vcf.gz
	 vcfkeepsamples ${B3}_${B4}_Somatic.Anno.GTFIX.vcf.gz $B3 | bgzip -c > ${B3}_Somatic.Anno.GTFIX.vcf.gz
	 vcfkeepsamples ${B2}_${B4}_Somatic.Anno.GTFIX.vcf.gz $B4 | bgzip -c > ${B4}_Somatic.Anno.GTFIX.vcf.gz
	 tabix -f ${B1}_Somatic.Anno.GTFIX.vcf.gz;tabix -f ${B2}_Somatic.Anno.GTFIX.vcf.gz;tabix -f ${B3}_Somatic.Anno.GTFIX.vcf.gz;tabix -f ${B4}_Somatic.Anno.GTFIX.vcf.gz
done

vcfkeepsamples LUAN1_LUAN3_Somatic.Anno.GTFIX.vcf.gz LUAN1 |bgzip -fc > LUAN1_Somatic.Anno.GTFIX.vcf.gz
vcfkeepsamples LUAN2_LUAN3_Somatic.Anno.GTFIX.vcf.gz LUAN2 | bgzip -fc > LUAN2_Somatic.Anno.GTFIX.vcf.gz
vcfkeepsamples LUAN2b_LUAN3_Somatic.Anno.GTFIX.vcf.gz LUAN2b | bgzip -fc > LUAN2b_Somatic.Anno.GTFIX.vcf.gz
vcfkeepsamples LUAN4b_LUAN3_Somatic.Anno.GTFIX.vcf.gz LUAN4b | bgzip -fc > LUAN4b_Somatic.Anno.GTFIX.vcf.gz
vcfkeepsamples LUAN3_LUAN4_Somatic.Anno.GTFIX.vcf.gz LUAN4 | bgzip -fc > LUAN4_Somatic.Anno.GTFIX.vcf.gz
vcfkeepsamples LUAN3_LUAN4b_Somatic.Anno.GTFIX.vcf.gz LUAN4b | bgzip -fc > LUAN4b_Somatic.Anno.GTFIX.vcf.gz

for i in `ls LUAN*_Somatic.Anno.GTFIX.vcf.gz`; do tabix -f $i; done

vcfkeepsamples DEIV1_DEIV3_Somatic.Anno.GTFIX.vcf.gz DEIV1| bgzip -fc > DEIV1_Somatic.Anno.GTFIX.vcf.gz
vcfkeepsamples DEIV2_DEIV3_Somatic.Anno.GTFIX.vcf.gz DEIV2| bgzip -fc > DEIV2_Somatic.Anno.GTFIX.vcf.gz
vcfkeepsamples DEIV2b_DEIV3_Somatic.Anno.GTFIX.vcf.gz DEIV2b| bgzip -fc > DEIV2b_Somatic.Anno.GTFIX.vcf.gz
vcfkeepsamples DEIV1_DEIV2_DEIV2b_Somatic.Anno.GTFIX.vcf.gz DEIV3| bgzip -fc > DEIV3_Somatic.Anno.GTFIX.vcf.gz
vcfkeepsamples DEIV1_DEIV2_DEIV2b_Somatic.Anno.GTFIX.vcf.gz DEIV4| bgzip -fc > DEIV4_Somatic.Anno.GTFIX.vcf.gz

for i in `ls DEIV*_Somatic.Anno.GTFIX.vcf.gz`; do tabix -f $i; done

cat Names.txt | while read i; do D=${i}_Somatic.Anno.GTFIX.vcf.gz;R=${D/1_/2_};echo $R; bcftools isec -C $R $D -p ${i};done
bcftools isec  -C DEIV2b_Somatic.Anno.GTFIX.vcf.gz DEIV1_Somatic.Anno.GTFIX.vcf.gz -p DEIV2b1
bcftools isec  -C DEIV2b_Somatic.Anno.GTFIX.vcf.gz DEIV2_Somatic.Anno.GTFIX.vcf.gz -p DEIV2b

bcftools isec  -C LUAN2_Somatic.Anno.GTFIX.vcf.gz LUAN1_Somatic.Anno.GTFIX.vcf.gz -p LUAN2
bcftools isec  -C LUAN2b_Somatic.Anno.GTFIX.vcf.gz LUAN1_Somatic.Anno.GTFIX.vcf.gz -p LUAN2b1
bcftools isec  -C LUAN2b_Somatic.Anno.GTFIX.vcf.gz LUAN2_Somatic.Anno.GTFIX.vcf.gz -p LUAN2b

##########################################################################################################################################
##########################################GERMLINE PER MINORI#############################################################################

cd /lustre2/scratch/Vago/161/Exome/2017/REL_vs_DIAG_Somatic

for i in `ls *3_*4_Somatic.Anno.GTFIX.vcf.gz`; do printf "%s\t%s\t"  $i ;zgrep -c -v "#" $i; done
# ALFE3_ALFE4_Somatic.Anno.GTFIX.vcf.gz           5842
# BESU3_BESU4_Somatic.Anno.GTFIX.vcf.gz           10418
# CALU3_CALU4_Somatic.Anno.GTFIX.vcf.gz           4279
# DEIV3_DEIV4_Somatic.Anno.GTFIX.vcf.gz           11597
# DEST3_DEST4_Somatic.Anno.GTFIX.vcf.gz           6380
# DR43_DR44_Somatic.Anno.GTFIX.vcf.gz             5359
# DR53_DR54_Somatic.Anno.GTFIX.vcf.gz             5014
# FOCA3_FOCA4_Somatic.Anno.GTFIX.vcf.gz           8999
# GAGRA3_GAGRA4_Somatic.Anno.GTFIX.vcf.gz         8662
# GUAL3_GUAL4_Somatic.Anno.GTFIX.vcf.gz           5230
# LUAN3_LUAN4_Somatic.Anno.GTFIX.vcf.gz           5472
# LUAN3_LUAN4b_Somatic.Anno.GTFIX.vcf.gz          8835
# MABI3_MABI4_Somatic.Anno.GTFIX.vcf.gz           5795
# MOGE3_MOGE4_Somatic.Anno.GTFIX.vcf.gz           9079
# PIAG3_PIAG4_Somatic.Anno.GTFIX.vcf.gz           9437
# PRELU3_PRELU4_Somatic.Anno.GTFIX.vcf.gz         8004


for i in {ALFE,BESU,CALU,DEST,DR4,DR5,FOCA,GAGRA,GUAL,MABI,MOGE,PIAG,PRELU}
do
	 A=${i}3_${i}4_Somatic.Anno.GTFIX.vcf.gz
	 B3=`echo ${i}3`
	 vcfkeepsamples ${A} $B3|vcfsort | vcffixup - |snpSift extractFields -e "." - \
	 "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
	 "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
	 "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
	 "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
	 "GEN[*].GT" \
	 "GEN[*].RO" \
	 "GEN[*].AO" \
	 "GEN[*].QA" \
	 |sed -e "s/1\/1/'1\/1/g" > ${i}_Germline_vs_Donor.tsv
done

for i in {LUAN,DEIV}
do
	 A=${i}3_${i}4_Somatic.Anno.GTFIX.vcf.gz
	 B3=`echo ${i}3`
	 vcfkeepsamples ${A} $B3|vcfsort | vcffixup - |snpSift extractFields -e "." - \
	 "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
	 "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
	 "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
	 "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
	 "GEN[*].GT" \
	 "GEN[*].RO" \
	 "GEN[*].AO" \
	 "GEN[*].QA" \
	 |sed -e "s/1\/1/'1\/1/g" > ${i}_Germline_vs_Donor.tsv
done

for i in LUAN
do
	 A=${i}3_${i}4b_Somatic.Anno.GTFIX.vcf.gz
	 B3=`echo ${i}3`
	 vcfkeepsamples ${A} $B3|vcfsort | vcffixup - |snpSift extractFields -e "." - \
	 "CHROM" "POS" "REF" "ALT" "PLOIDY" "TYPE" "ID" "QUAL" "EFF[0].GENE" \
	 "ANN[0].GENEID" "ANN[0].FEATUREID"      "AA" "ANN[0].HGVS" \
	 "KGPhase1" "KGPhase3" "CAF" "COMMON" "G5A" "dbNSFP_ESP6500_AA_AF" "dbNSFP_ESP6500_EA_AF" "dbNSFP_ExAC_AF" \
	 "EFF[0].ALLELE" "EFF[0].EFFECT" "EFF[0].IMPACT" "EFF[0].FEATURE" "ANN[0].BIOTYPE" \
	 "GEN[*].GT" \
	 "GEN[*].RO" \
	 "GEN[*].AO" \
	 "GEN[*].QA" \
	 |sed -e "s/1\/1/'1\/1/g" > ${i}_Germline_vs_Donorb.tsv
done

#######################################################################################################

cat Names.txt | while read i; do D=${i};R=${D/1_/2_};echo $R;grep -c -v "#" ${i}/0000.vcf;done

cat Names.txt | while read i; do D=${i};R=${D/1_/2_}; cat ${i}/0000.vcf|  snpSift filter "COMMON"|snpSift extractFields - "EFF[0].EFFECT" "EFF[0].GENE"| grep -v synon|cut -f 2| grep -v "^EFF"; done| sort -u 

 scompare WT1, qualcosa non va.
  zgrep WT1 *2_*3_*.vcf.gz| cut -f 1
DEIV2_DEIV3_Somatic.Anno.GTFIX.vcf.gz:chr1
DEIV2_DEIV3_Somatic.Anno.vcf.gz:chr1
DR42_DR43_Somatic.Anno.GTFIX.vcf.gz:chr11
DR42_DR43_Somatic.Anno.vcf.gz:chr11
DR52_DR53_Somatic.Anno.GTFIX.vcf.gz:chr11
DR52_DR53_Somatic.Anno.vcf.gz:chr11
GUAL2_GUAL3_Somatic.Anno.GTFIX.vcf.gz:chr11
GUAL2_GUAL3_Somatic.Anno.GTFIX.vcf.gz:chr11
GUAL2_GUAL3_Somatic.Anno.vcf.gz:chr11
GUAL2_GUAL3_Somatic.Anno.vcf.gz:chr11
MOGE2_MOGE3_Somatic.Anno.GTFIX.vcf.gz:chr11
MOGE2_MOGE3_Somatic.Anno.GTFIX.vcf.gz:chr11
MOGE2_MOGE3_Somatic.Anno.vcf.gz:chr11
MOGE2_MOGE3_Somatic.Anno.vcf.gz:chr11
PIAG2_PIAG3_Somatic.Anno.GTFIX.vcf.gz:chr11
PIAG2_PIAG3_Somatic.Anno.vcf.gz:chr11
