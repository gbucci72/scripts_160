bamFile=$1 #file list of bams 
##########SORTED BY COORDINATE#####

script_name=${bamFile%%.list}.sh
currentDirectory=`pwd`
BAM_DIR=${currentDirectory}/MARKED
METRICS_BAM_DIR=${currentDirectory}/COVERAGE
mkdir -p ${METRICS_BAM_DIR}

cat <<__EOF__> $script_name

#!/bin/bash
#PBS -N ${script_name:0:5}
#PBS -l select=1:ncpus=6:mem=18g:app=java
#PBS -V
#PBS -Wsandbox=private
#PBS -q workq
##PBS -M bucci.gabriele@hsr.it

rm /tmp/gbucci/*

cd $PWD

condactivate

java -Xmx16g -Djava.io.tmpdir=/lustre2/scratch/gbucci/tmp \
-jar /lustre1/ctgb_usr/miniconda3/envs/gatk-3.6/opt/gatk-3.6/GenomeAnalysisTK.jar \
-T DepthOfCoverage \
-R /lustre1/genomes/hg19/fa/hg19.fa \
-I $bamFile  \
-o COVERAGE_DEPTH.cov  \
-omitBaseOutput \
-geneList /lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Regions.refSeq  \
-ct 2 -ct 20 -ct 30 -ct 60 -ct 90 -ct 150 -ct 300 -ct 400 \
-L /lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Amplicons.interval_list 

condeactivate 

__EOF__


qsub $script_name

