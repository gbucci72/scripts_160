#FLT3 inhibitor 
#IL15 IRF7 ATF4
#Target=Gencode19+50bp

cd /lustre2/scratch/Vago/161/TestGene
freebayes -f /lustre1/genomes/hg19/fa/hg19.fa -F 0.01 -q 20 -m 30 -p 6 -C 2 -t Genes.bed /lustre1/workspace/Ciceri/161_Leukemia/EXOME/BAM/*bam > Genes.vcf 
freebayes -f /lustre1/genomes/hg19/fa/hg19.fa -F 0.01 -q 20 -m 30 -C 2 -t Genes.bed /lustre1/workspace/Ciceri/161_Leukemia/EXOME/BAM/*bam > Genes.vcf 

targ=Genes.vcf
bgzip -f $targ
targ=${targ}.gz
tabix -f $targ


zcat $targ |vcfbreakmulti| vcfsort| java -jar /usr/local/cluster/src/snpEff/SnpSift.jar filter \
"( DP > 10 ) & ( MQM[ANY] > 20 & MQMR[ANY] > 20 ) &  \
((RPL[ANY] > 1) | (RPR[ANY] > 1)) & (SAF[ANY] > 0) & (SAR[ANY] > 0)"  | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar filter \
"(( GEN[ANY].DP[ANY] > 10 ) & \
  (GEN[ANY].AO[ANY] > 5) & ( GEN[ANY].QA[ANY] > 30 ))" | \
bgzip -c > ${targ%%.vcf.gz}.multi.unBias.Qual.vcf.gz

tabix ${targ%%.vcf.gz}.multi.unBias.Qual.vcf.gz 

targ=${targ%%.vcf.gz}.multi.unBias.Qual.vcf.gz 
dbSNP=/lustre1/genomes/hg19/annotation/dbSNP-146.vcf.gz
Cosmic=/lustre1/genomes/hg19/annotation/v75_CosmicCodingMuts.vcf.gz
Clinvar=/lustre1/genomes/hg19/annotation/clinvar_20160802.vcf.gz
Exac=/lustre1/genomes/hg19/annotation/ExAC.r0.3.1.sites.vep.vcf.gz
dbNSFP=/lustre1/genomes/hg19/annotation/dbNSFP2.9.txt.gz

zcat $targ |java -jar /usr/local/cluster/src/snpEff/SnpSift.jar annotate $dbSNP | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar annotate $Exac | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar annotate $Clinvar | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar annotate  $Cosmic| \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar dbnsfp -db $dbNSFP - | \
vcfsort| bgzip -f -c > ${targ%%.vcf.gz}.Anno.vcf.gz

tabix ${targ%%.vcf.gz}.Anno.vcf.gz 

zcat ${targ%%.vcf.gz}.Anno.vcf.gz| java -jar /usr/local/cluster/src/snpEff/snpEff.jar eff \
-c  /lustre1/tools/etc/snpEff.config -t -noLog -nodownload -v -lof GRCh37.75 > ${targ%%.vcf.gz}.Anno.Eff.vcf
bgzip -f ${targ%%.vcf.gz}.Anno.Eff.vcf
tabix -f ${targ%%.vcf.gz}.Anno.Eff.vcf.gz

#CASE CONTROL
#Germline
snpSift caseControl -name "Germline" -tfam Germline.ped.txt Genes.vcf.gz
#Donor





snpSift extractFields - -s "|" -e "NA" \
"CHROM" "POS" "REF" "ALT" "ID" "QUAL" \
"QR" "QA" "DP" "RO" "AO" "SAF" "SAR" "AB" "ABP" "RPL" "RPR" "EPP" "EPPR" "DPRA" "TYPE" "CIGAR" "MQM" "MQMR" \
"KGPhase1" "KGPhase3" "KG_AC" "AN_POPMAX" "AC_POPMAX" "KG_AF_GLOBAL" "KG_AF_POPMAX" "POPMAX" "G5" "G5A" "CAF" "MUT" "SAO" "PM"  "FS"  \
"clinvar_pathogenic" "clinvar_mut" "CLNHGVS" "ANN[0].GENE" "STRAND" "ANN[0]" "GEN[*].GT[*]"   | less -S

snpSift extractFields - -s "|" -e "NA" \
"CHROM" "POS" "REF" "ALT" "ID" "QUAL" \
"QR" "QA" "DP" "RO" "AO" "TYPE" "MQM" "MQMR" \
"KGPhase1" "KGPhase3" "KG_AC" "AN_POPMAX" "AC_POPMAX" "KG_AF_GLOBAL" "KG_AF_POPMAX" "POPMAX" "G5" "G5A" "CAF" "MUT" "SAO" "PM"  "FS"  \
"clinvar_pathogenic" "clinvar_mut" "CLNHGVS" "ANN[0].GENE" "STRAND" "ANN[0]" "GEN[*].GT[*]"   | less -S


