FAM=$1
name=${FAM%%_*}

echo $name

cat <<__EOF__> $name.txt
#####################################################
track ${name}
type bam
shortLabel ${name}_DNA
longLabel  ${name} hg19 aligned reads
visibility hide
maxHeightPixels 500:100:8
viewLimits 1:20
priority 1
bamColorMode tag
bamColorTag XM
bigDataUrl  

########################################################

__EOF__


#for i in `ls *1_Allfiles_sorted.bw`; do echo $i; /lustre2/scratch/Vago/161/Exome/2017/vago_161/Hub_tracks.sh $i; done

#http://user1:grT7DsacXsdvT3a01@predictors.ricerca.hsr.it/shared_data/shared/gbucci/Ciceri/PanelAML/hub.txt
http://genome.ucsc.edu/cgi-bin/hgTracks?db=hg38&hgS_loadUrlName=http://user1:grT7DsacXsdvT3a01@predictors.ricerca.hsr.it/shared_data/shared/gbucci/Ciceri/PanelAML/hub.txt