#PBS -l select=1:app=java:ncpus=4:mem=16gb
#PBS -P 160 
#PBS -V
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
targ=$arg1
name=$arg2


zcat $targ | vcffilter -f " ( AO > 0 ) " | \
vcffilter -f " ( DP > 3 ) & (QUAL > 0 ) & ( SAF > 0 & SAR > 0  ) & ( RPR > 0 | RPL > 0 ) & ( MQM > 20 & MQMR > 20 )" | \
java -jar /usr/local/cluster/src/snpEff/SnpSift.jar filter \
"(( GEN[ANY].DP[ANY] >= 4 ) & (GEN[ANY].AO[ANY] >= 1) & ( GEN[ANY].QA[ANY] > 0 ))" | \
bgzip -c > ${targ%%.vcf.gz}.unBias.vcf.gz

tabix ${targ%%.vcf.gz}.unBias.vcf.gz



###for i in `ls Merged.chr*.vcf.gz`; do qsub -v arg1="$i" -N ${i:0:6} ~/PBSScripts/SNPSIFTANNO.sh ; done
