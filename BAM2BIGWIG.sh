##BAM to BIGWIG##

#!/bin/bash

file=$1	#BAM
L=$2 #left extension in bp
R=$3 #right extension in bp
tab=$4 #genome tab chromsizes
name=${file%%_*}

user=`whoami`

 
BWA=/usr/local/cluster/bin/bwa
SAM=/usr/local/cluster/bin/samtools
script_name=${name}_HS


cat <<__EOF__> ${script_name}.sh
HOST=`hostname`

#PBS -N $script_name
#PBS -l select=1:ncpus=2:mem=4g:app=java
#PBS -V
#PBS -o ${name}_${HOST}.log
#PBS -e ${name}_${HOST}.err
#PBS -q workq
#PBS -M bucci.gabriele@hsr.it
#PBS -m ae
#PBS -Wsandbox=private

cd $PWD
#HOST=`hostname`
ulimit -l unlimited
ulimit -s unlimited
echo $HOST

samtools view -q 15 -F 0x400 -u $file | \
bedtools bamtobed -i stdin | \
bedtools slop -i stdin -l $L -r $R -g $tab | \
bedtools genomecov -g $tab -i stdin -bg | \
wigToBigWig stdin $tab ${file%%.bam}.bigwig


__EOF__

qsub ${script_name}.sh

