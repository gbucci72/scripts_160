#HSMETRCIS
setwd("~/Documents/Projects/ISO/Ciceri/Vago/160_Relapsing_Leukemia/Pannello_Haloplex/HSMETRICS")
list.files()
HEAD<-read.csv("HS1.PRE.hsmetrics.tsv",sep="\t")

HS1Pre<-read.csv("HS1.PRE.hsmetrics.tsv",header=T,sep="\t")
HS1Post<-read.csv("HS1.POST.hsmetrics.tsv",header=T,sep="\t")
HS2Pre<-read.csv("HS2.PRE.hsmetrics.tsv",header=T,sep="\t")
HS2Post<-read.csv("HS2.POST.hsmetrics.tsv",header=T,sep="\t")

boxplot(main="Mean On Target",cbind(HS1Pre[,23],HS1Post[,23],HS2Pre[,23],HS2Post[,23]),outline=F,sub="MBC: HS1Pre HS1Post HS2Pre HS2Post")

Names<-as.character(HS1Post[,1])
Names<-(gsub(pattern = ".hsmetrics",replacement = "",Names))

pdf("HS1_HS2_Hsmetrics.pdf")
  Names<-as.character(HS1Post[,1])
  Names<-(gsub(pattern = ".hsmetrics",replacement = "",Names))
  matplot(y=cbind(round(HS1Pre[,23],digits = 0),round(HS1Post[,23],digits = 0)),type = "l",
          xlab="",ylab="Mean Target Coverage",ylim=c(0,1000),xaxt="n",main="HS1")
  axis(side=1,at=(1:length(Names)),labels=Names,tick = F,las=2,cex.axis=0.5)
  abline(h = 0,col="black")
  abline(h = 20,col="green")
  axis(side=2,at=20,labels = "20x",pos = 1,hadj = -0.2,col="green",col.axis="green")
  
  matplot(y=cbind(HS1Pre[,30],HS1Post[,30]),type = "l",xlab="",ylim=c(0,1),ylab="PCT of 10x target bases covered",xaxt="n",main="HS1")
  axis(side=1,at=(1:length(Names)),labels=Names,tick = F,las=2,cex.axis=0.5)
  abline(h = 0,col="black")
  abline(h = 0.95,col="green")
  axis(side=2,at=0.95,labels = "95%",pos = 1,hadj = -0.2,col="green",col.axis="green")
  
  matplot(y=cbind(HS1Pre[,31],HS1Post[,31]),type = "l",xlab="",ylim=c(0,1),ylab="PCT of 20x target bases covered",xaxt="n",main="HS1")
  axis(side=1,at=(1:length(Names)),labels=Names,tick = F,las=2,cex.axis=0.5)
  abline(h = 0,col="black")
  abline(h = 0.95,col="green")
  axis(side=2,at=0.95,labels = "95%",pos = 1,hadj = -0.2,col="green",col.axis="green")
  
  Names<-as.character(HS2Post[,1])
  Names<-(gsub(pattern = ".hsmetrics",replacement = "",Names))
  matplot(y=cbind(round(HS2Pre[,23],digits = 0),round(HS2Post[,23],digits = 0)),type = "l",
          xlab="",ylab="Mean Target Coverage",ylim=c(0,1000),xaxt="n",main="HS2")
  axis(side=1,at=(1:length(Names)),labels=Names,tick = F,las=2,cex.axis=0.5)
  abline(h = 20,col="green")
  abline(h = 0,col="black")
  axis(side=2,at=20,labels = "20x",pos = 1,hadj = -0.2,col="green",col.axis="green")
  
  
  matplot(y=cbind(HS2Pre[,30],HS2Post[,30]),type = "l",xlab="",ylim=c(0,1),ylab="PCT of 10x target bases covered",xaxt="n",main="HS1")
  axis(side=1,at=(1:length(Names)),labels=Names,tick = F,las=2,cex.axis=0.5)
  abline(h = 0,col="black")
  abline(h = 0.95,col="green")
  axis(side=2,at=0.95,labels = "95%",pos = 1,hadj = -0.2,col="green",col.axis="green")
  
  matplot(y=cbind(HS2Pre[,31],HS2Post[,31]),type = "l",xlab="",ylim=c(0,1),ylab="PCT of 20x target bases covered",xaxt="n",main="HS1")
  axis(side=1,at=(1:length(Names)),labels=Names,tick = F,las=2,cex.axis=0.5)
  abline(h = 0,col="black")
  abline(h = 0.95,col="green")
  axis(side=2,at=0.95,labels = "95%",pos = 1,hadj = -0.2,col="green",col.axis="green")

dev.off()



plot(HS1Pre[-58,23],HS1Post[-58,23])
barplot(HS1Pre[,23]/HS1Post[,23])

plot(log2(HS2Pre[,23]),log2(HS2Post[,23]))
