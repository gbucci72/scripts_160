cd /lustre2/scratch/Vago/160/Haloplex/160630_SN859_0309_AHJ2KVBCXX/Agilent_MBC_Mark/FASTQ/BBDUK/BAM/CLIPPED_NM7/MARKED/CHUNKS/BYSAMPLE/


for i in `ls *.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.vcf.gz`;
do vcfkeepinfo $i "AF" "AC" "FILTER" | vcfkeepgeno - "DP" "RO" "AO" | \
snpSift filter "GEN[ANY].AO >= 1" > ${i%%.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.vcf.gz}.vcf ; done &

for i in `ls *vcf`; do bgzip -f $i && tabix -f ${i}.gz; done

mkdir New_Combined_Tier3
cat Names.txt| while read i; do mv ${i}.vcf.gz* New_Combined_Tier3; done


vcf-merge *.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.vcf.gz| vcfbreakmulti| vcfsort | vcfuniq |vcffixup - | bgzip -fc > Combined_Tier3.vcf.gz && tabix -f  Combined_Tier3.vcf.gz

cd New_Combined_Tier3

snpSift filter " GEN[*].AO[*] > 1 " ../Combined_Tier3.vcf.gz | vcfkeepinfo - AF | vcfkeepgeno - AO | vcf2bed > Combined_Tier3.bed
vcfintersect -b Combined_Tier3.bed ../Multi.IN.LCR.unBias.Anno.Eff.En.vcf.gz |bgzip -fc > Combined_Tier3.vcf.gz && tabix -fc Combined_Tier3.vcf.gz

zcat Combined_Tier3.vcf.gz| vcffilter -g " AO > 0 " | snpSift extractFields - "CHROM" "POS" "ALT" "GEN[*].AO[*]"| grep -v "#"|awk '{OFS="\t";print $1,$2-1,$2,$1"_"$2-1"_"$2"_"$3,NF-3}' > Combined_Tier3_AC.bed
#snpSift filter " GEN[*].AO[*] > 1 " Combined_Tier3.vcf.gz | snpSift extractFields - "CHROM" "POS" "ALT" "GEN[*].AO[*]"| grep -v "#"|awk '{OFS="\t";print $1,$2-1,$2,$1"_"$2-1"_"$2"_"$3,NF-3}' > Combined_Tier3_AC.bed


sort -u -k 1,1 -k 2,2n -k 4,4 Combined_Tier3_AC.bed > Combined_Tier3.bed

#zcat ../Combined_Tier3.vcf.gz | vcf2bed --do-not-sort  | awk '{OFS="\t"; print $1,$2,$3,$1"_"$2"_"$3"_"$7}'> Combined_Tier3_AC.bed 
#paste -d "\t" Combined_Tier3_AC.txt Combined_Tier3_AC.bed | awk '{OFS="\t"; print $2,$3,$4,$5,$1}' | sort -u -k 1,1 -k 2,2n -k 4,4  > Combined_Tier3.bed

bgzip -f Combined_Tier3.bed;tabix -f Combined_Tier3.bed.gz

cat Names.txt | while read i;
do zcat ${i}.vcf.gz | vcf2bed --do-not-sort| awk '{OFS="_"; print $1,$2,$3,$7}' > ${i}_AC.txt
KASP=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/KASP.bed
zgrep -w -f ${i}_AC.txt Combined_Tier3.bed.gz| awk '{OFS="\t"; print $1,$2,$3,$4"_"$5}' | bgzip -cf > ${i}.bed.gz; tabix -f  ${i}.bed.gz
vcfannotate -k HS2_AC -b <(zcat ${i}.bed.gz) ../${i}.Multi.IN.LCR.unBias.Anno.Eff.En.Pre.tier3.MBC.vcf.gz | vcfannotate -k KASP -b $KASP| snpSift filter "GEN[0].AO >= 1 " > ../${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2AC.vcf
bgzip -f ../${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2AC.vcf
tabix -f ../${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2AC.vcf.gz
done

##HLALOSS
vcfkeepsamples ../Combined_Tier3.vcf.gz `cat HLALOSS.txt` > ../HLALOSS.vcf
bgzip ../HLALOSS.vcf; tabix ../HLALOSS.vcf.gz
#snpSift filter " GEN[*].AO[*] > 1 "  ../HLALOSS.vcf.gz |snpSift extractFields - "CHROM" "POS" "ALT" "GEN[*].AO[*]"| grep -v "#"|awk '{OFS="\t";print $1,$2-1,$2,$1"_"$2-1"_"$2"_"$3,NF}' > HLALOSS_AC.bed
zcat ../HLALOSS.vcf.gz |vcffilter -g " AO > 0 " | snpSift extractFields - "CHROM" "POS" "ALT" "GEN[*].AO[*]"| grep -v "#"|awk '{OFS="\t";print $1,$2-1,$2,$1"_"$2-1"_"$2"_"$3,NF-3}' > HLALOSS_AC.bed
#zcat ../HLALOSS.vcf.gz | vcf2bed --do-not-sort  | awk '{OFS="\t"; print $1,$2,$3,$1"_"$2"_"$3"_"$7}'> HLALOSS_AC.bed
#paste -d "\t" HLALOSS_AC.txt HLALOSS_AC.bed | awk '{OFS="\t"; print $2,$3,$4,$5,$1}' | sort -u -k 1,1 -k 2,2n -k 4,4  > HLALOSS.bed
sort -u -k 1,1 -k 2,2n -k 4,4 HLALOSS_AC.bed > HLALOSS.bed
bgzip HLALOSS.bed;tabix HLALOSS.bed.gz

cat Names.txt | while read i;
do zgrep -w -f ${i}_AC.txt HLALOSS.bed.gz| awk '{OFS="\t"; print $1,$2,$3,$4"_"$5}' | bgzip -cf > ${i}.HL.bed.gz; tabix -f  ${i}.HL.bed.gz
vcfannotate -k HLALOSS_AC -b <(zcat ${i}.HL.bed.gz) ../${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2AC.vcf.gz | snpSift filter "GEN[0].AO >= 1 " > ../${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2ACHL.vcf
bgzip -f ../${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2ACHL.vcf
tabix -f ../${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2ACHL.vcf.gz
done

#Calculate VAF
cd /lustre2/scratch/Vago/160/Haloplex/160630_SN859_0309_AHJ2KVBCXX/Agilent_MBC_Mark/FASTQ/BBDUK/BAM/CLIPPED_NM7/MARKED/CHUNKS/BYSAMPLE/

cat Names.txt | while read i;
do zcat ${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS2ACHL.vcf.gz| sed 's/HS2_AC/HS1_AC/g' - | \
bgzip -fc > ${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS1ACHL.vcf.gz; tabix -f ${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS1ACHL.vcf.gz 
done 

#BLOCK
cat Names.txt | while read i;
do snpSift extractFields ${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS1ACHL.vcf.gz \
-e '.' "CHROM" "POS" "ID" "QUAL" "AO" "RV" "REF" "ALT" "VARTYPE" \
"KGPhase1" "KGPhase3" "COMMON" "HLALOSS_AC" "PREMBC" "dbNSFP_ExAC_AF[0]" \
"dbNSFP_ExAC_AC[0]" "dbNSFP_CADD_phred" "dbNSFP_ESP6500_AA_AF" "dbNSFP_FATHMM_pred" \
"dbNSFP_MutationTaster_pred" "G5A" "PM" "PMC" "OM" "GENE" "ABP" "CIGAR" "GEN[0].AO[0]" \
"GEN[0].RO[0]" "GEN[0].DP[0]" "GQ" "GL" "QA" "ANN[0].GENE" "EFF[0]"| grep -v "#" \
|awk '{OFS="\t"; print $28/($30+0.0001)}' > ${i}.vaf
printf "%s\n" "#VAF" `cat ${i}.vaf` > ${i}.vaf
snpSift extractFields ${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS1ACHL.vcf.gz \
-e '.' "CHROM" "POS" "ID" "QUAL" "AO" "RV" "REF" "ALT" "VARTYPE" "KGPhase1" "KGPhase3" \
"COMMON" "PREMBC" "dbNSFP_ExAC_AF[0]" "dbNSFP_ExAC_AC[0]" "dbNSFP_CADD_phred" \
"dbNSFP_ESP6500_AA_AF" "dbNSFP_FATHMM_pred" "dbNSFP_MutationTaster_pred" "G5A" \
"PM" "PMC" "OM" "GENE" "ABP" "CIGAR" "GEN[0].AO[0]" "GEN[0].RO[0]" "GEN[0].QA[0]" \
"GEN[0].DP[0]" "GEN[0].GQ[0]" "GEN[0].GL[0]" "KASP" "ANN[0].GENE" "EFF[0]" > ${i}.tsv
snpSift extractFields ${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS1ACHL.vcf.gz -e '0' "HS1_AC" | cut -d_ -f 5 | grep -v '^$' | cut -d: -f 1| sed '1s/^/#HS1_AC\n/' > ${i}.ac
snpSift extractFields ${i}.Multi.IN.LCR.unBias.tier3.vcf.MBC.HS1ACHL.vcf.gz -e '0' "HLALOSS_AC" | cut -d_ -f 5  | grep -v '^$' | cut -d: -f 1| sed '1s/^/#HLALOSS_AC\n/' > ${i}.HL.ac
paste -d "\t" ${i}.ac ${i}.HL.ac ${i}.vaf ${i}.tsv > ${i}.tier3.tsv
done &
#BLOCK
