#!/bin/bash

R1=$1	#BAM lane1
R2=$2	#BAM lane2

File=$R1

ID=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level1; done)`
library=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level2; done)`
lane=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level3; done)`
name=${ID}_${library}_${lane}

user=`whoami`

 
BWA=/usr/local/cluster/bin/bwa
SAM=/usr/local/cluster/bin/samtools
picard=/usr/local/cluster/bin/picard.jar

script_name=${ID}_MD


cat <<__EOF__> ${script_name}.sh
HOST=`hostname`

#PBS -N $script_name
#PBS -l select=1:ncpus=2:mem=8g:app=java
#PBS -V
#PBS -o ${name}_${HOST}.log
#PBS -e ${name}_${HOST}.err
#PBS -q workq
#PBS -M bucci.gabriele@hsr.it
#PBS -m ae
#PBS -Wsandbox=private

cd $PWD
#HOST=`hostname`
ulimit -l unlimited
ulimit -s unlimited
echo $HOST

java -jar /usr/local/cluster/bin/picard.jar MarkDuplicates \
    I=$R1 \
    I=$R2 \
    O=${name}.merged.bam \
    M=${name}.metrics.txt \
    VALIDATION_STRINGENCY=SILENT

__EOF__

qsub ${script_name}.sh
