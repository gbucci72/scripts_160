#PBS -l select=1:app=java:ncpus=2:mem=18gb
#PBS -P 160 
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae

##CWD=`pwd`
CWD=$PBS_O_WORKDIR
cd $CWD 
i=$arg1
name=${i%%.bam}

samtools view -b -F 772 -q 1 ${i}| samtools sort  -T ${i%%.bam}.tmp -@ 2 - -o ${i%%.bam}_filtered.bam 

samtools index ${i%%.bam}_filtered.bam 

java -Xmx12G -jar /lustre1/tools/bin/GenomeAnalysisTK-2.1.8.jar \
-T ClipReads \
-R /lustre1/genomes/hs37d5/fa/hs37d5.fa \
-o ${i%%.bam}_filtered_clipped.bam \
-I ${i%%.bam}_filtered.bam  \
-CT "1-5,245-250" \
-QT 10 \
-CR WRITE_Q0S 


