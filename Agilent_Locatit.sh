#PBS -l select=1:app=java:ncpus=2:mem=32gb
#PBS -P 160 
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae
##CWD=`pwd`

CWD=$PBS_O_WORKDIR
cd $CWD 

i=$arg1
index=$arg2

name=${i%%_*}

condactivate gatk-3.6

AMP=/lustre2/scratch/Vago/160/Haloplex/Gene_Panel/Design/25069-1432135121_Amplicons.bed
echo $name
java -Xmx16G -jar /lustre2/scratch/gbucci/LocatIt_v3.5.1.46.jar -D -U -IB -b $AMP -X TEMP_${name} -o ${name} ${i} ./${index} 1> $PBS_JOBID.out 2> $PBS_JOBID.err

/usr/local/cluster/bin/samtools sort ${name}.bam -T ${name}_tmp -o ${name}_sorted.bam

/usr/local/cluster/bin/samtools index ${name}_sorted.bam 

condeactivate

#for i in `ls *_filtered_clipped.bam`;do Index=${i%%_*}*_I2_*.fastq.gz; qsub -v arg1="$i",arg2="$Index" -N BC_${i:0:6} /lustre2/scratch/Vago/160/Haloplex/scripts_160/Agilent_Locatit.sh;done