#!/bin/bash

vcf=$1
#region=$2

script_name=${vcf%%_*}.sh 

cat <<__EOF__> $script_name
HOST=`hostname`
echo $HOST

#PBS -N ${script_name}
#PBS -l select=1:ncpus=2:mem=6g:app=java
#PBS -o ${name}_${HOST}.log
#PBS -e ${name}_${HOST}.err
#PBS -q workq
#PBS -M bucci.gabriele@hsr.it
#PBS -m ae
#PBS -Wsandbox=private


cd $PWD

ulimit -l unlimited
ulimit -s unlimited

condactivate freebayes

zcat $vcf| snpSift filter "(QUAL > 0.1)&(GEN[ANY].AO>1)"|\
vcfbreakmulti | bcftools norm -f /lustre1/genomes/hg19/fa/hg19.fa | bgzip -fc > ${vcf%%.vcf.gz}_norm.vcf.gz

tabix  -f ${vcf%%.vcf.gz}_norm.vcf.gz

condeactivate

__EOF__

qsub $script_name
