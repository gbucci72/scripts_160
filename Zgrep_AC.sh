#!/bin/bash

i=$1


script_name=${i%%_*}.sh 

cat <<__EOF__> $script_name
HOST=`hostname`
echo $HOST

#PBS -N ${script_name}
#PBS -l select=1:ncpus=2:mem=6g:app=java
#PBS -o ${name}_${HOST}.log
#PBS -e ${name}_${HOST}.err
#PBS -q workq
#PBS -M bucci.gabriele@hsr.it
#PBS -m ae
#PBS -Wsandbox=private


cd $PWD

ulimit -l unlimited
ulimit -s unlimited

zgrep -w -f ${i}_AC.txt Combined_Tier3.bed.gz > ${i}.txt

__EOF__

qsub $script_name
